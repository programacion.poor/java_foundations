import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class ejercicio04 {

    public static int calcularPermutaciones(int n) {
        if (n == 0) {
            return 1;
        } else {
            return n * calcularPermutaciones(n - 1);
        }
    }

    public static void main(String[] args) {
        Scanner scanner;
        try {
            scanner = new Scanner(new File("datos_generados.txt"));
        } catch (FileNotFoundException e) {
            System.out.println("Archivo no encontrado.");
            return;
        }

        ArrayList<Integer> numeros = new ArrayList<Integer>();
        while (scanner.hasNext()) {
            if (scanner.hasNextInt()) {
                numeros.add(scanner.nextInt());
            } else {
                scanner.next();
            }
        }
        scanner.close();

        System.out.print("Ingrese la cantidad de números aleatorios a obtener: ");
        Scanner inputScanner = new Scanner(System.in);
        int n = inputScanner.nextInt();
        inputScanner.close();

        Collections.shuffle(numeros);

        int[] seleccionados = new int[n];
        for (int i = 0; i < n; i++) {
            seleccionados[i] = numeros.get(i);
        }

        System.out.println("\nLos números seleccionados son:");
        for (int i = 0; i < n; i++) {
            System.out.println(seleccionados[i]);
        }

        long numPermutaciones = 1;
        for (int i = 1; i <= n; i++) {
            numPermutaciones *= i;
        }
        System.out.println("Número de permutaciones de los números seleccionados: " + numPermutaciones);
    }

}
