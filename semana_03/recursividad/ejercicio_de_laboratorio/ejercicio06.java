import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.Scanner;

public class ejercicio06 {

    public static void main(String[] args) {

        Random random = new Random();
        ArrayList<Integer> datos = new ArrayList<>();
        for (int i = 0; i < 1000; i++) {
            datos.add(random.nextInt(301));
        }

        try {
            FileWriter writer = new FileWriter("datos_generados.txt");
            for (int dato : datos) {
                writer.write(dato + "\n");
            }
            writer.close();
        } catch (IOException e) {
            System.out.println("Error al guardar los datos generados.");
            e.printStackTrace();
            return;
        }

        int suma = 0;
        int maximo = Integer.MIN_VALUE;
        int minimo = Integer.MAX_VALUE;
        double media, varianza, desviacionEstandar;
        ArrayList<Integer> moda = new ArrayList<>();
        for (int i = 0; i < 301; i++) {
            moda.add(0);
        }
        for (int dato : datos) {
            suma += dato;
            maximo = Math.max(maximo, dato);
            minimo = Math.min(minimo, dato);
            moda.set(dato, moda.get(dato) + 1);
        }
        media = (double) suma / datos.size();
        varianza = 0;
        for (int dato : datos) {
            varianza += Math.pow(dato - media, 2);
        }
        varianza /= datos.size();
        desviacionEstandar = Math.sqrt(varianza);

        ArrayList<Integer> modaIndices = new ArrayList<>();
        int maxModa = Collections.max(moda);
        for (int i = 0; i < moda.size(); i++) {
            if (moda.get(i) == maxModa) {
                modaIndices.add(i);
            }
        }

        try {
            FileWriter writer = new FileWriter("resultados.txt");
            writer.write("Valor máximo: " + maximo + "\n");
            writer.write("Valor mínimo: " + minimo + "\n");
            writer.write("Promedio: " + media + "\n");
            writer.write("Moda: " + modaIndices + "\n");
            writer.write("Desviación estándar: " + desviacionEstandar + "\n");
            writer.close();
        } catch (IOException e) {
            System.out.println("Error al guardar los resultados.");
            e.printStackTrace();
            return;
        }

        System.out.println("Los resultados se han guardado en el archivo \"resultados.txt\".");

    }

}
