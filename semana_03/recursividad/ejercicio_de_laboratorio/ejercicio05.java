import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ejercicio05 {
    private String tipo;
    private int parametro1;
    private int parametro2;

    public ejercicio05(String tipo, int parametro1, int parametro2) {
        this.tipo = tipo;
        this.parametro1 = parametro1;
        this.parametro2 = parametro2;
    }

    public void construir() {
        switch (tipo) {
            case "sierpinski":
                construirSierpinski(parametro1);
                break;

            default:
                System.out.println("Tipo de fractal no válido");
        }
    }

    private void construirSierpinski(int n) {
        if (n == 0) {
            System.out.println("*");
        } else {
            construirSierpinski(n - 1);
            for (int i = 0; i < Math.pow(2, n - 1); i++) {
                System.out.print(" ");
            }
            construirSierpinski(n - 1);
            System.out.println();
            for (int i = 0; i < Math.pow(2, n - 1); i++) {
                System.out.print(" ");
            }
            for (int i = 0; i < Math.pow(2, n - 1); i++) {
                System.out.print(" ");
            }
            construirSierpinski(n - 1);
        }
    }

    public static void main(String[] args) {
        Scanner wz;
        try {
            wz = new Scanner(new File("datos_generados.txt"));
        } catch (FileNotFoundException e) {
            System.out.println("Archivo no encontrado.");
            return;
        }
        String tipo = wz.nextLine();
        int parametro1 = wz.nextInt();
        int parametro2 = wz.nextInt();
        wz.close();

        ejercicio05 fractal = new ejercicio05(tipo, parametro1, parametro2);
        fractal.construir();
    }
}
