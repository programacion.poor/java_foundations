import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ejercicio02 {
    public static int gcd(int a, int b) {
        if (b == 0) {
            return a;
        } else {
            return gcd(b, a % b);
        }
    }

    public static int gcdSequence(Scanner scanner, int n) {
        if (n == 0) {
            return 0;
        } else {
            int number = scanner.nextInt();
            int result = gcd(number, gcdSequence(scanner, n - 1));
            System.out.println("El MCD parcial de los números hasta " + number + " es " + result);
            return result;
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese la cantidad de números aleatorios a seleccionar: ");
        int n = sc.nextInt();
        
        Scanner scanner;
        try {
            scanner = new Scanner(new File("datos_generados.txt"));
        } catch (FileNotFoundException e) {
            System.out.println("Archivo no encontrado.");
            return;
        }

        System.out.println("Calculando MCD recursivo de los " + n + " números aleatorios en el archivo...");
        int result = gcdSequence(scanner, n);
        System.out.println("El MCD de todos los números es " + result);

        scanner.close();
    }
}
