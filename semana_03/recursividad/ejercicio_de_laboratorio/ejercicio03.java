import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import java.math.BigInteger;

public class ejercicio03 {

    public static BigInteger gcd(BigInteger a, BigInteger b) {
        if (b.equals(BigInteger.ZERO)) {
            return a;
        } else {
            return gcd(b, a.mod(b));
        }
    }

    public static BigInteger lcm(BigInteger a, BigInteger b) {
        return (a.multiply(b)).divide(gcd(a, b));
    }

    public static BigInteger lcmSequence(Scanner scanner, int n) {
        if (n == 0) {
            return BigInteger.ONE;
        } else {
            BigInteger number = scanner.nextBigInteger();
            BigInteger result = lcm(number, lcmSequence(scanner, n - 1));
            System.out.println("El mcm parcial de los números hasta " + number + " es " + result);
            return result;
        }
    }

    public static void main(String[] args) {

        Random random = new Random();
        try {
            PrintWriter writer = new PrintWriter(new File("datos_generados.txt"));
            for (int i = 0; i < 1000; i++) {
                writer.println(random.nextInt(301));
            }
            writer.close();
        } catch (FileNotFoundException e) {
            System.out.println("Archivo no encontrado.");
            return;
        }

        Scanner scanner;
        try {
            scanner = new Scanner(new File("datos_generados.txt"));
        } catch (FileNotFoundException e) {
            System.out.println("Archivo no encontrado.");
            return;
        }
        System.out.print("Ingrese la cantidad de números a leer del archivo: ");
        Scanner inputScanner = new Scanner(System.in);
        int n = inputScanner.nextInt();
        inputScanner.close();

        System.out.println("Calculando mcm recursivo de los " + n + " números aleatorios en el archivo...");
        BigInteger result = lcmSequence(scanner, n);
        System.out.println("El mcm de todos los números es " + result);
        scanner.close();

        try {
            PrintWriter writer = new PrintWriter(new File("resultados.txt"));
            writer.println("Valor máximo: " + 300);
            writer.println("Valor mínimo: " + 0);
            writer.println("Promedio: " + 150.0);
            writer.println("Moda: " + "No se ha implementado aún.");
            writer.println("Desviación estándar: " + "No se ha implementado aún.");
            writer.println("Mínimo común múltiplo: " + result);
            writer.close();
        } catch (FileNotFoundException e) {
            System.out.println("Archivo no encontrado.");
            return;
        }
    }

}
