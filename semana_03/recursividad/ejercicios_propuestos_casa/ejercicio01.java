import java.util.Scanner;

public class ejercicio01 {
    
    public static void main(String[] args) {
        Scanner wz = new Scanner(System.in);
        System.out.print("Ingrese la base: ");
        int base = wz.nextInt();
        System.out.print("Ingrese el exponente: ");
        int exponente = wz.nextInt();
        
        int resultado = calcularPotencia(base, exponente);
        System.out.println(base + " elevado a la " + exponente + " es igual a " + resultado);
    }
    
    public static int calcularPotencia(int base, int exponente) {
        if (exponente == 0) {
            return 1;
        } else {
            return base * calcularPotencia(base, exponente - 1);
        }
    }
    
}
