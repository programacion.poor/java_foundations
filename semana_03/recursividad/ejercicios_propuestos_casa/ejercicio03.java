import java.util.Scanner;

public class ejercicio03 {
    
    public static void main(String[] args) {
        Scanner wz = new Scanner(System.in);
        System.out.print("Ingrese el valor de m: ");
        int m = wz.nextInt();
        System.out.print("Ingrese el valor de n: ");
        int n = wz.nextInt();
        System.out.print("Ingrese el valor de k: ");
        int k = wz.nextInt();
        
        int resultado = calcularAckermann(m, n, k);
        System.out.println("El valor de Ackermann(" + m + ", " + n + ") mod " + k + " es: " + resultado);
    }
    
    public static int calcularAckermann(int m, int n, int k) {
        if (m == 0) {
            return (n + 1) % k;
        } else if (n == 0) {
            return calcularAckermann(m - 1, 1, k);
        } else {
            int x = calcularAckermann(m, n - 1, k);
            return calcularAckermann(m - 1, x, k);
        }
    }
    
}
