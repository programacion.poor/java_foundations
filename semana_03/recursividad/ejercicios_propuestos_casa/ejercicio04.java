import java.math.BigInteger;
import java.util.Scanner;

public class ejercicio04 {
    
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingrese un número entero positivo: ");
        BigInteger n = scanner.nextBigInteger();
        scanner.close();
        
        System.out.println("Los números del 1 al " + n + " en orden inverso son:");
        imprimirNumerosInversos(n);
    }
    
    public static void imprimirNumerosInversos(BigInteger n) {
        if (n.equals(BigInteger.ONE)) {
            System.out.println(n);
        } else {
            System.out.println(n);
            imprimirNumerosInversos(n.subtract(BigInteger.ONE));
        }
    }
    
}
