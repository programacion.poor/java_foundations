import java.util.Scanner;

public class ejercicio02 {
    
    public static void main(String[] args) {
        Scanner wz = new Scanner(System.in);
        System.out.print("Ingrese la longitud del array: ");
        int n = wz.nextInt();
        int[] array = new int[n];
        for (int i = 0; i < n; i++) {
            System.out.print("Ingrese el valor para el elemento " + i + ": ");
            array[i] = wz.nextInt();
        }
        
        int maximo = encontrarMaximo(array);
        System.out.println("El valor máximo del array es: " + maximo);
    }
    
    public static int encontrarMaximo(int[] array) {
        return encontrarMaximoRecursivo(array, 0, array.length - 1);
    }
    
    private static int encontrarMaximoRecursivo(int[] array, int inicio, int fin) {
        if (inicio == fin) {
            return array[inicio];
        } else {
            int mitad = (inicio + fin) / 2;
            int maxIzquierdo = encontrarMaximoRecursivo(array, inicio, mitad);
            int maxDerecho = encontrarMaximoRecursivo(array, mitad + 1, fin);
            return Math.max(maxIzquierdo, maxDerecho);
        }
    }
    
}
