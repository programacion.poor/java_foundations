import java.util.Scanner;

public class ejercicio05 {

    public static void main(String[] args) {
        Scanner wz = new Scanner(System.in);
        System.out.print("Ingrese la cantidad de filas del triángulo: ");
        int n = wz.nextInt();
        wz.close();
        dibujarTriangulo(n, 0);
    }

    public static void dibujarTriangulo(int n, int espacios) {
        if (n == 0) {
            return;
        }
        dibujarTriangulo(n - 1, espacios + 1);
        for (int i = 0; i < espacios; i++) {
            System.out.print(" ");
        }
        for (int i = 0; i < n; i++) {
            System.out.print("*");
        }
        System.out.println();
    }
}

