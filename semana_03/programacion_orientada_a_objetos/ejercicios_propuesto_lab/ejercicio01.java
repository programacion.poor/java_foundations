/**
 * Esta clase define objetos de tipo Persona con un nombre, apellidos,
 * número de documento de identidad, año de nacimiento, país de nacimiento
 * y género.
 *
 * @version 1.2/2023
 */
public class ejercicio01 {
  String nombre; // Atributo que identifica el nombre de una persona
  String apellidos; // Atributo que identifica los apellidos de una persona
  String númeroDocumentoIdentidad; // Atributo que identifica el número de documento de identidad de una persona
  int añoNacimiento; // Atributo que identifica el año de nacimiento de una persona
  String paísNacimiento; // Atributo que identifica el país de nacimiento de una persona
  char género; // Atributo que identifica el género de una persona ('H' o 'M')

  /**
   * Constructor de la clase Persona
   *
   * @param nombre                   Parámetro que define el nombre de la persona
   * @param apellidos                Parámetro que define los apellidos de la
   *                                 persona
   * @param númeroDocumentoIdentidad Parámetro que define el número del documento
   *                                 de identidad de la persona
   * @param añoNacimiento            Parámetro que define el año de nacimiento de
   *                                 la persona
   * @param paísNacimiento           Parámetro que define el país de nacimiento de
   *                                 la persona
   * @param género                   Parámetro que define el género de la persona
   */
  ejercicio01(String nombre, String apellidos, String númeroDocumentoIdentidad, int añoNacimiento,
      String paísNacimiento, char género) {
    this.nombre = nombre;
    this.apellidos = apellidos;
    this.númeroDocumentoIdentidad = númeroDocumentoIdentidad;
    this.añoNacimiento = añoNacimiento;
    this.paísNacimiento = paísNacimiento;
    this.género = género;
  }

  /**
   * Método que imprime en pantalla los valores de los atributos del objeto.
   */
  void imprimir() {
    System.out.println("Nombre: " + nombre);
    System.out.println("Apellidos: " + apellidos);
    System.out.println("Documento de identidad: " + númeroDocumentoIdentidad);
    System.out.println("Año de nacimiento: " + añoNacimiento);
    System.out.println("País de nacimiento: " + paísNacimiento);
    System.out.println("Género: " + género);
  }

  public static void main(String[] args) {
    ejercicio01 persona1 = new ejercicio01("Juan", "Pérez", "12345678", 1990, "Argentina", 'H');
    ejercicio01 persona2 = new ejercicio01("María", "González", "87654321", 1995, "México", 'M');

    System.out.println("Persona 1:");
    persona1.imprimir();
    System.out.println();

    System.out.println("Persona 2:");
    persona2.imprimir();
    System.out.println();
  }
}
