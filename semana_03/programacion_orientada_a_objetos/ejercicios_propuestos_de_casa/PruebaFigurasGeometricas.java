public class PruebaFigurasGeometricas {
    public static void main(String[] args) {
        Circulo circulo = new Circulo(5);
        System.out.println("Área del círculo: " + circulo.calcularArea());
        System.out.println("Perímetro del círculo: " + circulo.calcularPerimetro());

        Rectangulo rectangulo = new Rectangulo(4, 6);
        System.out.println("Área del rectángulo: " + rectangulo.calcularArea());
        System.out.println("Perímetro del rectángulo: " + rectangulo.calcularPerimetro());

        Cuadrado cuadrado = new Cuadrado(3);
        System.out.println("Área del cuadrado: " + cuadrado.calcularArea());
        System.out.println("Perímetro del cuadrado: " + cuadrado.calcularPerimetro());

        TrianguloRectangulo triangulo = new TrianguloRectangulo(5, 12);
        System.out.println("Área del triángulo rectángulo: " + triangulo.calcularArea());
        System.out.println("Perímetro del triángulo rectángulo: " + triangulo.calcularPerimetro());
        System.out.println("Hipotenusa del triángulo rectángulo: " + triangulo.calcularHipotenusa());
        System.out.println("Tipo de triángulo: " + triangulo.determinarTipoTriangulo());
    }
}