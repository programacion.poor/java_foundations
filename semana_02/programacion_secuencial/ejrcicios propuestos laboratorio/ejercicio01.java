import java.util.Scanner;

public class ejercicio01 {
    public static void main(String[] args) {
        Scanner wz = new Scanner(System.in);


        int[] arr = new int[2];
        for (int i = 0; i < 2; i++) {
            System.out.print("Ingrese un numero " + (i+1) + ": ");
            arr[i] = wz.nextInt();
        }

        int s = sumArregl(arr);

        System.out.println("La suma de los numeros es: " + s);
    }

    public static int sumArregl(int[] arr) {
        int s = 0;
        for (int i = 0; i < arr.length; i++) {
            s += arr[i];
        }
        return s;
    }
}
