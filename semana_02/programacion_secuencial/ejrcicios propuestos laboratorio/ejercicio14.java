import java.util.Scanner;

public class ejercicio14 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num;

        do {
            System.out.print("Ingresa un número entre 1000 y 7000: ");
            num = sc.nextInt();
        } while (num < 1000 || num > 7000);

        int[] digitos = new int[4];
        for (int i = 0; i < 4; i++) {
            digitos[i] = num % 10;
            num /= 10;
        }

        System.out.println("Salida:");
        for (int i = 3; i >= 0; i--) {
            System.out.println("[" + i + "] = " + digitos[i]);
        }
    }

}
