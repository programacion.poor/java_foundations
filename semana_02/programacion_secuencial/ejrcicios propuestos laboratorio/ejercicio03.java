import java.util.Scanner;

public class ejercicio03 {
    public static void main(String[] args) {
        Scanner wz = new Scanner(System.in);

        System.out.print("Ingrese el número de estudiantes: ");
        int n = wz.nextInt();

        String[][] Estud = new String[n][2];

        for (int i = 0; i < n; i++) {
            System.out.print("Ingrese el nombre del estudiante " + (i+1) + ": ");
            Estud[i][0] = wz.next();
            System.out.print("Ingrese la edad del estudiante " + (i+1) + ": ");
            Estud[i][1] = wz.next();
        }

        double eddP = CalPr(Estud);

        System.out.println("La edad promedio de los Estud es: " + eddP);
    }

    public static double CalPr(String[][] Estud) {
        int SumEdd = 0;
        for (int i = 0; i < Estud.length; i++) {
            SumEdd += Integer.parseInt(Estud[i][1]);
        }
        return (double) SumEdd / Estud.length;
    }
}
