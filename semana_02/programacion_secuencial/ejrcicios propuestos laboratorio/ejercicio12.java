import java.util.Scanner;

public class ejercicio12 {
    public static void main(String[] args) {
        Scanner wz = new Scanner(System.in);

        System.out.println("¿Está lloviendo? (si/no)");
        String llvia = wz.nextLine().toLowerCase();

        if (llvia.equals("si")) {
            System.out.println("¿Está haciendo mucho viento? (si/no)");
            String vnt = wz.nextLine().toLowerCase();

            if (vnt.equals("si")) {
                System.out.println("Hace mucho vnt para salir con una sombrilla.");
            } else {
                System.out.println("Lleva una sombrilla por si acaso.");
            }
        } else if (llvia.equals("no")) {
            System.out.println("¡Que tengas un bonito día!");
        } else {
            System.out.println("Respuesta no válida.");
        }
    }
}
