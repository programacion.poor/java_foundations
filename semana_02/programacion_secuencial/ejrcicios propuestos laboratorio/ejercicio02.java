import java.util.Scanner;

public class ejercicio02 {
    public static void main(String[] args) {
        Scanner wz = new Scanner(System.in);

        int[] arreglo = new int[2];
        for (int i = 0; i < 2; i++) {
            System.out.print("Ingrese el valor del elemento " + (i+1) + ": ");
            arreglo[i] = wz.nextInt();
        }

        int P = PrA(arreglo);

        System.out.println("El Producto de los elementos del arreglo es: " + P);
    }

    public static int PrA(int[] arreglo) {
        int P = 1;
        for (int i = 0; i < arreglo.length; i++) {
         P *= arreglo[i];
        }
        return P;
    }
}
