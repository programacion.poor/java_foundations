import java.util.Scanner;

public class ejercicio04 {
    public static void main(String[] args) {
        Scanner wz = new Scanner(System.in);
        
        System.out.println("Ingresa varios numeros separados por espacios:");
        String numeros = wz.nextLine();
        String[] NumSep = numeros.split(" ");
        
        int[] arrgl = new int[NumSep.length];
        for (int i = 0; i < NumSep.length; i++) {
            arrgl[i] = Integer.parseInt(NumSep[i]);
        }
        
        int max = enctrmax(arrgl);
        System.out.println("El máximo elemento del arreglo es: " + max);
    }

    public static int enctrmax(int[] arrgl) {
        int max = arrgl[0];
        for (int i = 1; i < arrgl.length; i++) {
            if (arrgl[i] > max) {
                max = arrgl[i];
            }
        }
        return max;
    }
}
