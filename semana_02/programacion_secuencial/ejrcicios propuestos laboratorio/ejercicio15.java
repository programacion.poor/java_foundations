import java.util.Scanner;

public class ejercicio15 {
    public static void main(String[] args) {
        Scanner wz = new Scanner(System.in);
        
        System.out.println("Ingresa un número entero:");
        int num = wz.nextInt();
        
        String numRmn = convertirARomanos(num);
        System.out.println(num + " en números romanos es: " + numRmn);
    }

    public static String convertirARomanos(int num) {
        int[] vlrs = { 1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1 };
        String[] simbolos = { "M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I" };

        StringBuilder result = new StringBuilder();
        int i = 0;

        while (num > 0) {
            if (num >= vlrs[i]) {
                result.append(simbolos[i]);
                num -= vlrs[i];
            } else {
                i++;
            }
        }

        return result.toString();
    }
}
