import java.util.Scanner;
import java.util.Random;

public class ejercicio11 {
    public static void main(String[] args) {
        Scanner wz = new Scanner(System.in);
        Random rand = new Random();
        
        System.out.print("Ingresa el tamaño del arreglo: ");
        int tmñ = wz.nextInt();
        
        int[] num = new int[tmñ];
        
        for (int i = 0; i < num.length; i++) {
            num[i] = rand.nextInt(300) + 1;
        }
        
        System.out.print("Ingresa el dígito final que deseas buscar: ");
        int digit = wz.nextInt();
        
        if (digit >= 0 && digit <= 9) {
            int[] numFilt = filtrarPorDigito(num, digit);
            
            System.out.println("Números aleatorios generados:");
            mostrarnum(num);
            
            System.out.println("Números que terminan en " + digit + ":");
            mostrarnum(numFilt);
        } else {
            System.out.println("El dígito ingresado no es válido.");
        }
    }
    
    public static int[] filtrarPorDigito(int[] arreglo, int digit) {
        int[] numFilt = new int[arreglo.length];
        int contador = 0;
        
        for (int i = 0; i < arreglo.length; i++) {
            if (arreglo[i] % 10 == digit) {
                numFilt[contador] = arreglo[i];
                contador++;
            }
        }
        
        int[] result = new int[contador];
        
        for (int i = 0; i < contador; i++) {
            result[i] = numFilt[i];
        }
        
        return result;
    }
    
    public static void mostrarnum(int[] arreglo) {
        for (int i = 0; i < arreglo.length; i++) {
            System.out.print(arreglo[i] + " ");
        }
        System.out.println();
    }
}
