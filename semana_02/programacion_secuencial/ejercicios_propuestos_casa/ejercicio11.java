public class ejercicio11 {

    public static void main(String[] args) {
        int[] numeros = {5, 10, 15, 20, 25};
        double media = encontrarMedia(numeros);
        System.out.println("La media es: " + media);
    }

    public static double encontrarMedia(int[] numeros) {
        int suma = 0;
        for (int i = 0; i < numeros.length; i++) {
            suma += numeros[i];
        }
        double media = (double)suma / numeros.length;
        return media;
    }
}
