import java.util.Arrays;
import java.util.Scanner;

public class ejercicio14 {
    public static void main(String[] args) {
        Scanner wz = new Scanner(System.in);

        System.out.print("Ingrese el tamaño del arreglo: ");
        int n = wz.nextInt();
        int[] arreglo = new int[n];
        System.out.println("Ingrese los elementos del arreglo en orden ascendente: ");
        for (int i = 0; i < n; i++) {
            arreglo[i] = wz.nextInt();
        }

        System.out.print("Ingrese el número a buscar: ");
        int numeroABuscar = wz.nextInt();

        int indice = Arrays.binarySearch(arreglo, numeroABuscar);
        if (indice >= 0) {
            int contador = 1;

            for (int i = indice - 1; i >= 0 && arreglo[i] == numeroABuscar; i--) {
                contador++;
            }

            for (int i = indice + 1; i < n && arreglo[i] == numeroABuscar; i++) {
                contador++;
            }
            System.out.println("El número " + numeroABuscar + " aparece " + contador + " veces en el arreglo.");
        } else {
            System.out.println("El número " + numeroABuscar + " no está en el arreglo.");
        }
    }
}
