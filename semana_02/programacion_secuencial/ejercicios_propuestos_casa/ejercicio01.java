public class ejercicio01 {
    public static void main(String[] args) {
        int[][] matriz = new int[7][7];
        int valor = 1;


        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[0].length; j++) {
                matriz[i][j] = valor;
                valor++;
            }
        }

        System.out.println("Matriz original:");
        mostrarMatriz(matriz);

        int[][] matrizRotada = new int[7][7];
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[0].length; j++) {
                matrizRotada[j][matriz.length - 1 - i] = matriz[i][j];
            }
        }

        System.out.println("Matriz rotada 90 grados en sentido horario:");
        mostrarMatriz(matrizRotada);
    }

    public static void mostrarMatriz(int[][] matriz) {
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[0].length; j++) {
                System.out.print(matriz[i][j] + "\t");
            }
            System.out.println();
        }
        System.out.println();
    }
}
