import java.util.Arrays;

public class ejercicio08 {

    public static void main(String[] args) {

        int[] arreglo = new int[10];
        for (int i = 0; i < arreglo.length; i++) {
            arreglo[i] = (int) (Math.random() * 100);
        }
        System.out.println("Arreglo: " + Arrays.toString(arreglo));

        int count = encontrarParejasPrimos(arreglo);
        System.out.println("Número de parejas de elementos cuya suma es un número primo: " + count);
    }

    public static boolean esPrimo(int numero) {
        if (numero <= 1) {
            return false;
        }
        for (int i = 2; i <= Math.sqrt(numero); i++) {
            if (numero % i == 0) {
                return false;
            }
        }
        return true;
    }

    public static int encontrarParejasPrimos(int[] arreglo) {
        int count = 0;
        for (int i = 0; i < arreglo.length - 1; i++) {
            for (int j = i + 1; j < arreglo.length; j++) {
                if (esPrimo(arreglo[i] + arreglo[j])) {
                    count++;
                    System.out.println("(" + arreglo[i] + ", " + arreglo[j] + ")");
                }
            }
        }
        return count;
    }
}
