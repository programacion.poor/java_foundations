
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.EigenDecomposition;

public class ejercicio04 {

    public static void main(String[] args) {
        double[][] matriz = {
            {1, 2, 3, 4, 5, 6, 7},
            {0, 1, 2, 3, 4, 5, 6},
            {0, 0, 1, 2, 3, 4, 5},
            {0, 0, 0, 1, 2, 3, 4},
            {0, 0, 0, 0, 1, 2, 3},
            {0, 0, 0, 0, 0, 1, 2},
            {0, 0, 0, 0, 0, 0, 1}
        };

        RealMatrix matrizReal = new Array2DRowRealMatrix(matriz);
        EigenDecomposition eig = new EigenDecomposition(matrizReal);
        RealMatrix jordan = eig.getV().multiply(eig.getD()).multiply(eig.getV().inverse());

        System.out.println(jordan.toString());
    }
}
