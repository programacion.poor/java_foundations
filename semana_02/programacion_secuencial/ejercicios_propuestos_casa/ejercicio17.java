import java.util.Arrays;
import java.util.Scanner;

public class ejercicio17 {
    public static void main(String[] args) {
        Scanner wz = new Scanner(System.in);

        // Pedir al usuario que ingrese el tamaño del arreglo y los elementos del arreglo
        System.out.print("Ingrese el tamaño del arreglo: ");
        int n = wz.nextInt();
        int[] arreglo = new int[n];
        System.out.println("Ingrese los elementos del arreglo: ");
        for (int i = 0; i < n; i++) {
            arreglo[i] = wz.nextInt();
        }

        // Dividir el arreglo en dos partes iguales
        int mitad = n / 2;
        int[] parte1 = Arrays.copyOfRange(arreglo, 0, mitad);
        int[] parte2 = Arrays.copyOfRange(arreglo, mitad, n);

        // Mostrar el resultado al usuario
        System.out.println("La primera parte del arreglo es " + Arrays.toString(parte1));
        System.out.println("La segunda parte del arreglo es " + Arrays.toString(parte2));
    }
}
