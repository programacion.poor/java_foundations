import java.util.Scanner;

public class ejercicio18 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Ingrese el tamaño del primer arreglo: ");
        int n1 = input.nextInt();
        int[] arreglo1 = new int[n1];
        System.out.println("Ingrese los elementos del primer arreglo: ");
        for (int i = 0; i < n1; i++) {
            arreglo1[i] = input.nextInt();
        }

        System.out.print("Ingrese el tamaño del segundo arreglo: ");
        int n2 = input.nextInt();
        int[] arreglo2 = new int[n2];
        System.out.println("Ingrese los elementos del segundo arreglo: ");
        for (int i = 0; i < n2; i++) {
            arreglo2[i] = input.nextInt();
        }

        int productoPunto = 0;
        int min = Math.min(n1, n2);
        for (int i = 0; i < min; i++) {
            productoPunto += arreglo1[i] * arreglo2[i];
        }

        System.out.println("El producto punto entre los dos arreglos es " + productoPunto);
    }
}
