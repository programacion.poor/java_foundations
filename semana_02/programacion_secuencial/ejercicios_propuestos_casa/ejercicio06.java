import java.util.Arrays;
import java.util.Comparator;

public class ejercicio06 {

    public static void main(String[] args) {
        String[] arreglo = {"manzana", "pera", "uva", "banana", "naranja"};

        Arrays.sort(arreglo, Comparator.reverseOrder());

        System.out.println(Arrays.toString(arreglo));
    }
}
