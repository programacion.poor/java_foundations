import java.util.Random;

public class ejercicio02 {
    public static void main(String[] args) {
        int[][] matriz = new int[7][7];
        Random rand = new Random();
        
        for(int i=0; i<matriz.length; i++) {
            for(int j=0; j<matriz[i].length; j++) {
                matriz[i][j] = rand.nextInt(100) + 1;
            }
        }
        
        for(int i=0; i<matriz.length; i++) {
            for(int j=0; j<matriz[i].length; j++) {
                System.out.print(matriz[i][j] + " ");
            }
            System.out.println();
        }
    }
}
