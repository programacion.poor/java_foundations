import java.util.Arrays;

public class ejercicio09 {
    public static void main(String[] args) {
        int[] arreglo = {5, 2, 8, 10, 1, 4, 7, 3, 6, 9};

        Arrays.sort(arreglo);

        int n = arreglo.length;

        if (n % 2 != 0) {

            int mediana = arreglo[n/2];
            System.out.println("La mediana es: " + mediana);
        } else {

            int mediana = (arreglo[n/2 - 1] + arreglo[n/2]) / 2;
            System.out.println("La mediana es: " + mediana);
        }
    }
}
