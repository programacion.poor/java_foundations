import java.util.Arrays;

public class ejercicio07 {

    public static void main(String[] args) {
        int[] numeros = {5, 10, 3, 8, 6, 1, 9, 2, 4, 7};
        int k = 4;
        int kEsimoElemento = encontrarKEsimoElemento(numeros, k);
        System.out.println("El " + k + "-ésimo elemento más grande es: " + kEsimoElemento);
    }

    public static int encontrarKEsimoElemento(int[] numeros, int k) {
        Arrays.sort(numeros);
        return numeros[numeros.length - k];
    }
}
