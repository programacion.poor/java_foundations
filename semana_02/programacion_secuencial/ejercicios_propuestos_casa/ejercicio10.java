import java.util.Arrays;

public class ejercicio10 {
    public static void main(String[] args) {
        int[] arreglo = {2, 4, 6, 8, 10, 2, 2, 8, 10, 10};
        int moda = encontrarModa(arreglo);
        System.out.println("La moda del arreglo es " + moda);
    }

    public static int encontrarModa(int[] arreglo) {

        Arrays.sort(arreglo);

        int moda = arreglo[0];
        int maxFrecuencia = 1;
        int frecuencia = 1;

        for (int i = 1; i < arreglo.length; i++) {
            if (arreglo[i] == arreglo[i - 1]) {
                frecuencia++;
            } else {
                if (frecuencia > maxFrecuencia) {
                    maxFrecuencia = frecuencia;
                    moda = arreglo[i - 1];
                }
                frecuencia = 1;
            }
        }

        if (frecuencia > maxFrecuencia) {
            maxFrecuencia = frecuencia;
            moda = arreglo[arreglo.length - 1];
        }

        return moda;
    }
}
