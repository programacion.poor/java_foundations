import java.util.HashMap;
import java.util.Scanner;

public class ejercicio15 {
    public static void main(String[] args) {
        Scanner wz = new Scanner(System.in);

        System.out.print("Ingrese el tamaño del arreglo: ");
        int n = wz.nextInt();
        int[] arreglo = new int[n];
        System.out.println("Ingrese los elementos del arreglo: ");
        for (int i = 0; i < n; i++) {
            arreglo[i] = wz.nextInt();
        }

        HashMap<Integer, Integer> contador = new HashMap<Integer, Integer>();
        for (int i = 0; i < n; i++) {
            if (contador.containsKey(arreglo[i])) {
                contador.put(arreglo[i], contador.get(arreglo[i]) + 1);
            } else {
                contador.put(arreglo[i], 1);
            }
        }

        int numeroMasRepetido = arreglo[0];
        int repeticionesDelNumeroMasRepetido = contador.get(arreglo[0]);
        for (int i = 1; i < n; i++) {
            if (contador.get(arreglo[i]) > repeticionesDelNumeroMasRepetido) {
                numeroMasRepetido = arreglo[i];
                repeticionesDelNumeroMasRepetido = contador.get(arreglo[i]);
            }
        }

        System.out.println("El número que se repite más veces es " + numeroMasRepetido +
                           ", que aparece " + repeticionesDelNumeroMasRepetido + " veces.");
    }
}
