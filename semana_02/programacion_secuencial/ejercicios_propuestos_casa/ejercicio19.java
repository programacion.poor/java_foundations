import java.util.Scanner;

public class ejercicio19 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Ingrese una letra: ");
        char letra = input.next().charAt(0);

        if (letra == 'a' || letra == 'e' || letra == 'i' || letra == 'o' || letra == 'u' ||
                letra == 'A' || letra == 'E' || letra == 'I' || letra == 'O' || letra == 'U') {
            System.out.println(letra + " es una vocal");
        } else {
            System.out.println(letra + " es una consonante");
        }
    }
}
