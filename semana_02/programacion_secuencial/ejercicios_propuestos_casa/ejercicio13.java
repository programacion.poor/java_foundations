import java.util.Scanner;

public class ejercicio13 {
   public static void main(String[] args) {
      Scanner input = new Scanner(System.in);

      System.out.print("Ingrese el tamaño del arreglo: ");
      int n = input.nextInt();

      int[] arreglo = new int[n];

      for (int i = 0; i < n; i++) {
         System.out.print("Ingrese el elemento " + (i+1) + " del arreglo: ");
         arreglo[i] = input.nextInt();
      }

      int contador = 0;
      for (int i = 0; i < n; i++) {
         if (arreglo[i] % 2 != 0) {
            contador++;
         }
      }

      System.out.println("El arreglo tiene " + contador + " números impares.");
   }
}
