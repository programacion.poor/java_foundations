import java.util.Arrays;

public class ejercicio05 {
    public static void main(String[] args) {
        int[] arreglo = {3, 1, 5, 2, 4, 9, 6, 8, 7};
        int[] subarreglo = encontrarSubarregloCreciente(arreglo);
        System.out.println(Arrays.toString(subarreglo));
    }
    
    public static int[] encontrarSubarregloCreciente(int[] arreglo) {
        int n = arreglo.length;
        int[] longitudSubsecuencia = new int[n];
        int[] indicesAnteriores = new int[n];
        Arrays.fill(longitudSubsecuencia, 1);
        Arrays.fill(indicesAnteriores, -1);
        int maxIndice = 0;
        int maxLongitud = 1;
        for (int i = 1; i < n; i++) {
            for (int j = 0; j < i; j++) {
                if (arreglo[j] < arreglo[i] && longitudSubsecuencia[j] >= longitudSubsecuencia[i]) {
                    longitudSubsecuencia[i] = longitudSubsecuencia[j] + 1;
                    indicesAnteriores[i] = j;
                    if (longitudSubsecuencia[i] > maxLongitud) {
                        maxLongitud = longitudSubsecuencia[i];
                        maxIndice = i;
                    }
                }
            }
        }
        int[] subarreglo = new int[maxLongitud];
        for (int i = maxIndice, j = maxLongitud - 1; i >= 0; i--) {
            if (longitudSubsecuencia[i] == j + 1) {
                subarreglo[j] = arreglo[i];
                j--;
            }
        }
        return subarreglo;
    }
}
