public class ejercicio20 {
    public static void main(String[] args) {
        int[] codigosASCII = new int[128];
        for (int i = 0; i < 128; i++) {
            codigosASCII[i] = i;
        }

        for (int i = 0; i < codigosASCII.length; i++) {
            System.out.println((char)codigosASCII[i] + ": " + codigosASCII[i]);
        }
    }
}
