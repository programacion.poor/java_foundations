public class ejercicio12 {
    public static void main(String[] args) {
        int[] arr = {2, 7, 1, 9, 3, 8, 6, 5, 4};
        int max = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] > max) {
                max = arr[i];
            }
        }
        System.out.println("El número más grande es: " + max);
    }
}
