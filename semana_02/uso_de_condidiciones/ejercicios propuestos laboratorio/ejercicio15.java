import java.util.Scanner;

public class ejercicio15 {
    public static void main(String[] args) {
        Scanner wz = new Scanner(System.in);
        System.out.print("Ingrese el número límite: ");
        int limite = wz.nextInt();
        
        int a = 2;
        int b = 1;
        int c = 0;
        
        System.out.print(a + " " + b + " ");
        
        while (c <= limite) {
            c = a + b;
            if (c <= limite) {
                System.out.print(c + " ");
            }
            a = b;
            b = c;
        }
    }
}
