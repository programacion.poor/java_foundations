import java.util.Scanner;

public class ejercicio11 {
    public static void main(String[] args) {
        Scanner wz = new Scanner(System.in);
        System.out.print("Ingrese el número de filas: ");
        int n = wz.nextInt();
        
        int altura = n/2 + 1;
        int espacios = n/2;
        int asteriscos = 1;
        
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= espacios; j++) System.out.print(" ");
            for (int j = 1; j <= asteriscos; j++) System.out.print("*");
            System.out.println();
            if (i < altura) {
                espacios--;
                asteriscos += 2;
            } else {
                espacios++;
                asteriscos -= 2;
            }
        }
    }
}
