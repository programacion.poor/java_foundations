import java.util.Scanner;

public class ejercicio02 {
    public static void main(String[] args) {
        Scanner wz = new Scanner(System.in);
        System.out.print("Introduce un número: ");
        
        int n = wz.nextInt();
        int ip = 0;
        int num = 1;
        
        for (int i = 0; i < n; i++) {
            ip += num;
            num += 2;
        }
        
        System.out.println("La suma de los " + n + " primeros números impares es: " + ip);

    }
}