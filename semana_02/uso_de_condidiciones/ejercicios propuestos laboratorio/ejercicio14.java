import java.util.Scanner;

public class ejercicio14 {
    public static void main(String[] args) {
        Scanner wz = new Scanner(System.in);
        System.out.print("Ingrese un número: ");
        int n = wz.nextInt();
        
        int factorial = 1;
        int i = 1;
        
        while (i <= n) {
            factorial *= i;
            i++;
        }
        
        System.out.println("El factorial de " + n + " es " + factorial);
    }
}
