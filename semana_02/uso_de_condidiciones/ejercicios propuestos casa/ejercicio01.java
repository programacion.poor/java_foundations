import java.util.Scanner;

public class ejercicio01 {

    public static void main(String[] args) {
        Scanner wz = new Scanner(System.in);
        System.out.print("Ingresa una palabra: ");
        String palabra = wz.nextLine();

        int i = 0;
        while (i < palabra.length()) {
            System.out.println("Letra " + (i + 1) + ": " + palabra.substring(i, i + 1));
            i++;
        }
    }
}
