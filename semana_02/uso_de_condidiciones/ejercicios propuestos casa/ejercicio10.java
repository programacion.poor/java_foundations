import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;

public class ejercicio10 {

    public static void main(String[] args) {
        int[] datos = leerDatos("datos.txt");
        double promedio = calcularPromedio(datos);
        int moda = calcularModa(datos);
        double desviacion = calcularDesviacion(datos, promedio);
        
        System.out.println("Promedio: " + promedio);
        System.out.println("Moda: " + moda);
        System.out.println("Desviación estándar: " + desviacion);
    }
    
    public static int[] leerDatos(String archivo) {
        int[] datos = null;
        try {
            Scanner wz = new Scanner(new File(archivo));
            int contador = 0;
            while (wz.hasNextInt()) {
                wz.nextInt();
                contador++;
            }
            wz.close();
            wz = new Scanner(new File(archivo));
            datos = new int[contador];
            for (int i = 0; i < contador; i++) {
                datos[i] = wz.nextInt();
            }
            wz.close();
        } catch (FileNotFoundException e) {
            System.out.println("No se pudo abrir el archivo " + archivo);
        }
        return datos;
    }
    
    public static double calcularPromedio(int[] datos) {
        int suma = 0;
        for (int i = 0; i < datos.length; i++) {
            suma += datos[i];
        }
        return (double) suma / datos.length;
    }
    
    public static int calcularModa(int[] datos) {
        Arrays.sort(datos);
        int moda = datos[0];
        int modaFrecuencia = 1;
        int frecuenciaActual = 1;
        for (int i = 1; i < datos.length; i++) {
            if (datos[i] == datos[i-1]) {
                frecuenciaActual++;
            } else {
                if (frecuenciaActual > modaFrecuencia) {
                    moda = datos[i-1];
                    modaFrecuencia = frecuenciaActual;
                }
                frecuenciaActual = 1;
            }
        }
        if (frecuenciaActual > modaFrecuencia) {
            moda = datos[datos.length-1];
            modaFrecuencia = frecuenciaActual;
        }
        return moda;
    }
    
    public static double calcularDesviacion(int[] datos, double promedio) {
        double suma = 0;
        for (int i = 0; i < datos.length; i++) {
            suma += Math.pow(datos[i] - promedio, 2);
        }
        return Math.sqrt(suma / datos.length);
    }

}
