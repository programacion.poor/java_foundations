import java.util.Scanner;

public class ejercicio06 {
    public static void main(String[] args) {
        Scanner wz = new Scanner(System.in);

        System.out.print("Ingresa el tamaño de la matriz: ");
        int size = wz.nextInt();

        int[] array = new int[size];

        System.out.println("Ingresa los elementos de la matriz: ");
        for (int i = 0; i < size; i++) {
            array[i] = wz.nextInt();
        }

        insertionSort(array);

        System.out.println("La matriz ordenada es: ");
        for (int i = 0; i < size; i++) {
            System.out.print(array[i] + " ");
        }
    }

    public static void insertionSort(int[] array) {
        for (int i = 1; i < array.length; i++) {
            int current = array[i];
            int j = i - 1;

            while (j >= 0 && array[j] > current) {
                array[j + 1] = array[j];
                j--;
            }

            array[j + 1] = current;
        }
    }
}
