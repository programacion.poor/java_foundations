import java.util.Scanner;

public class ejercicio17 {
    public static void main(String[] args) {
        Scanner wz = new Scanner(System.in);

        System.out.print("Ingresa el tamaño del vector: ");
        int n = wz.nextInt();

        int[] vector = new int[n];

        for (int i = 0; i < n; i++) {
            System.out.print("Ingresa el valor para la posición " + i + ": ");
            vector[i] = wz.nextInt();
        }

        int max = vector[0];
        int min = vector[0];

        for (int i = 1; i < n; i++) {
            if (vector[i] > max) {
                max = vector[i];
            }
            if (vector[i] < min) {
                min = vector[i];
            }
        }

        System.out.println("El valor máximo es: " + max);
        System.out.println("El valor mínimo es: " + min);
    }
}
