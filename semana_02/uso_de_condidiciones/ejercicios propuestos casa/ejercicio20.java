import java.util.Scanner;

public class ejercicio20 {
    public static void main(String[] args) {
        Scanner wz = new Scanner(System.in);
        System.out.print("Ingrese la cantidad de numeros perfectos a calcular: ");
        int n = wz.nextInt();

        int count = 0;
        int num = 1;
        int sum = 0;

        while (count < n) {
            if (esPerfecto(num)) {
                System.out.println(num);
                sum += num;
                count++;
            }
            num++;
        }

        System.out.println("La suma de los primeros " + n + " numeros perfectos es: " + sum);
    }

    public static boolean esPerfecto(int num) {
        int sum = 0;
        for (int i = 1; i <= num / 2; i++) {
            if (num % i == 0) {
                sum += i;
            }
        }
        return sum == num;
    }
}
