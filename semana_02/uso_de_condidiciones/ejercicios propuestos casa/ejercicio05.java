import java.util.Scanner;

public class ejercicio05 {
    public static void main(String[] args) {
        Scanner wz = new Scanner(System.in);

        System.out.print("Ingresa los argumentos separados por espacios: ");
        String input = wz.nextLine();
        String[] arguments = input.split(" ");

        int i = arguments.length - 1;
        while (i >= 0) {
            System.out.println(arguments[i]);
            i--;
        }
    }
}
