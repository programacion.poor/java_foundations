import java.util.Scanner;

public class ejercicio13 {
    public static void main(String[] args) {
        Scanner wz = new Scanner(System.in);

        System.out.print("Ingresa el número de segundos para el cronómetro: ");
        int tiempoTotal = wz.nextInt();

        for (int i = tiempoTotal; i >= 0; i--) {
            System.out.println("Tiempo restante: " + i + " segundos");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println("¡Tiempo terminado!");
    }
}
