import java.util.Scanner;

public class ejercicio11 {
    public static void main(String[] args) {
        Scanner wz = new Scanner(System.in);

        System.out.print("Ingrese el precio del producto: ");
        double precio = wz.nextDouble();

        System.out.print("Ingrese la cantidad de meses en los que se pagará: ");
        int meses = wz.nextInt();

        double totalPago = 0;
        double pagoMensual = precio / (Math.pow(2, meses) - 1);

        for (int i = 1; i <= meses; i++) {
            double pagoActual = pagoMensual * Math.pow(2, i-1);
            System.out.printf("Pago mes %d: S/%.2f\n", i, pagoActual);
            totalPago += pagoActual;
        }

        System.out.printf("Total pagado: S/%.2f\n", totalPago);
        System.out.printf("Pago mensual: S/%.2f\n", pagoMensual);
    }
}
