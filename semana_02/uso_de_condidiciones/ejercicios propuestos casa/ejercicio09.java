import java.util.Scanner;

public class ejercicio09 {
    public static void main(String[] args) {
        Scanner wz = new Scanner(System.in);

        System.out.print("Ingresa el primer término de la serie: ");
        double a = wz.nextDouble();

        System.out.print("Ingresa la razón de la serie: ");
        double r = wz.nextDouble();

        System.out.print("Ingresa el número de términos: ");
        int n = wz.nextInt();

        double suma = a * (1 - Math.pow(r, n)) / (1 - r);

        System.out.println("La suma de los primeros " + n + " términos de la serie es: " + suma);
    }
}
