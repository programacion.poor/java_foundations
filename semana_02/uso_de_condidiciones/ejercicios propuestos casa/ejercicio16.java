import java.util.Scanner;

public class ejercicio16 {
    public static void main(String[] args) {
        Scanner wz = new Scanner(System.in);

        System.out.print("Ingrese un número entero positivo: ");
        int n = wz.nextInt();

        for (int i = 1; i <= n; i += 2) {
            for (int j = i; j >= 1; j -= 2) {
                System.out.print(j + " ");
            }
            System.out.println();
        }
    }
}
