import java.util.*;

public class ejercicio07 {

    public static void main(String[] args) {
        Scanner wz = new Scanner(System.in);

        System.out.print("Ingresa el número de términos que quieres ver en la sucesión de Ulam: ");
        int n = wz.nextInt();

        List<Integer> ulam = new ArrayList<>();
        ulam.add(1);
        ulam.add(2);

        for (int i = 2; i < n; i++) {
            int count = 2;
            for (int j = 0; j < i && count <= 2; j++) {
                for (int k = j + 1; k < i && count <= 2; k++) {
                    if (ulam.get(j) + ulam.get(k) == ulam.get(i-1) && ulam.get(j) != ulam.get(k)) {
                        count++;
                    }
                }
            }
            if (count == 2) {
                ulam.add(i+1);
            } else {
                int j = ulam.get(i-1) + 1;
                boolean found = false;
                while (!found) {
                    count = 0;
                    for (int k = 0; k < i && count <= 1; k++) {
                        for (int l = k+1; l < i && count <= 1; l++) {
                            if (ulam.get(k) + ulam.get(l) == j && ulam.get(k) != ulam.get(l)) {
                                count++;
                            }
                        }
                    }
                    if (count == 1) {
                        ulam.add(j);
                        found = true;
                    } else {
                        j++;
                    }
                }
            }
        }

        System.out.println("Los primeros " + n + " términos de la sucesión de Ulam son:");
        for (int i = 0; i < n; i++) {
            System.out.print(ulam.get(i) + " ");
        }
    }
}
