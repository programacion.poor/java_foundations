import java.util.Scanner;

public class ejercicio19 {
    public static void main(String[] args) {
        Scanner wz = new Scanner(System.in);
        System.out.print("Ingrese la cantidad de números primos a imprimir: ");
        int n = wz.nextInt();
        int contador = 0;
        int numero = 2;
        
        while (contador < n) {
            boolean esPrimo = true;
            for (int i = 2; i <= Math.sqrt(numero); i++) {
                if (numero % i == 0) {
                    esPrimo = false;
                    break;
                }
            }
            if (esPrimo) {
                System.out.print(numero + " ");
                contador++;
            }
            numero++;
        }
    }
}
