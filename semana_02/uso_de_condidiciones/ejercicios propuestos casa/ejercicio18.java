import java.util.Scanner;

public class ejercicio18 {
    public static void main(String[] args) {
        Scanner wz = new Scanner(System.in);
        
        System.out.print("Ingrese el límite inferior del intervalo: ");
        int limiteInferior = wz.nextInt();
        
        int limiteSuperior;
        do {
            System.out.print("Ingrese el límite superior del intervalo: ");
            limiteSuperior = wz.nextInt();
        } while (limiteSuperior < limiteInferior);
        
        int sumaDentro = 0;
        int conteoFuera = 0;
        boolean igualLimiteInferior = false;
        boolean igualLimiteSuperior = false;
        
        int numero;
        do {
            System.out.print("Ingrese un número (0 para terminar): ");
            numero = wz.nextInt();
            if (numero != 0) {
                if (numero > limiteInferior && numero < limiteSuperior) {
                    sumaDentro += numero;
                } else if (numero == limiteInferior) {
                    igualLimiteInferior = true;
                } else if (numero == limiteSuperior) {
                    igualLimiteSuperior = true;
                } else {
                    conteoFuera++;
                }
            }
        } while (numero != 0);
        
        System.out.println("La suma de los números dentro del intervalo es: " + sumaDentro);
        System.out.println("Cantidad de números fuera del intervalo: " + conteoFuera);
        if (igualLimiteInferior) {
            System.out.println("Se ha introducido un número igual al límite inferior del intervalo.");
        }
        if (igualLimiteSuperior) {
            System.out.println("Se ha introducido un número igual al límite superior del intervalo.");
        }
    }
}
