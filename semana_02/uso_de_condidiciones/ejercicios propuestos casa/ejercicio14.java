import java.util.Scanner;

public class ejercicio14 {
    public static void main(String[] args) {
        Scanner wz = new Scanner(System.in);

        System.out.print("Ingresa un número entero positivo: ");
        int n = wz.nextInt();

        System.out.print("Los números impares desde 1 hasta " + n + " son: ");
        for (int i = 1; i <= n; i++) {
            if (i % 2 != 0) {
                System.out.print(i);
                if (i != n && i+1 % 2 != 0) {
                    System.out.print("; ");
                } else if (i != n && i+1 % 2 == 0) {
                    System.out.print(", ");
                }
            }
        }
    }
}
