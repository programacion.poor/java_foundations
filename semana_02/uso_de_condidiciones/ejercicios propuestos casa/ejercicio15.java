import java.util.Scanner;

public class ejercicio15 {
    public static void main(String[] args) {
        Scanner wz = new Scanner(System.in);

        System.out.print("Ingrese el monto del préstamo: ");
        double monto = wz.nextDouble();

        System.out.print("Ingrese la tasa de interés mensual (%): ");
        double tasa = wz.nextDouble();

        System.out.print("Ingrese el número de meses: ");
        int meses = wz.nextInt();

        double saldo = monto;
        for (int i = 1; i <= meses; i++) {
            saldo *= (1 + tasa/100);
            System.out.printf("Saldo después de %d meses: %.2f soles\n", i, saldo);
        }
    }
}
