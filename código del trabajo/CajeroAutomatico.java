import javax.swing.JOptionPane;

public class CajeroAutomatico {
    public static void main(String[] args) {
        // Variables de ejemplo para 5 clientes
        int[] dni = { 70758345, 87451264, 74523658, 71525421, 42154478 };
        String[] password = { "clave1", "clave2", "clave3", "clave4", "clave5" };
        double[] saldo = { 0, 0, 0, 0, 0 };

        // Ingreso de usuario
        int usuario = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingrese su DNI:"));
        int index = -1;

        // Búsqueda del usuario por DNI
        for (int i = 0; i < dni.length; i++) {
            if (dni[i] == usuario) {
                index = i;
                break;
            }
        }

        // Verificación de la contraseña
        int intentos = 0;
        if (index != -1) {
            do {
                String clave = JOptionPane.showInputDialog(null, "Ingrese su contraseña:");

                if (clave.equals(password[index])) {
                    JOptionPane.showMessageDialog(null, "¡Bienvenido " + usuario + "!");

                    // Menú principal
                    int opcion = 0;
                    do {
                        String[] opciones = { "Deposito", "Retiro", "Consulta de Saldo", "Salir" };
                        opcion = JOptionPane.showOptionDialog(null, "Elija la operación que desea realizar:",
                                "Menú Principal", JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null,
                                opciones, opciones[0]);

                        switch (opcion) {
                            case 0: // Deposito
                                String deposito = JOptionPane.showInputDialog(null, "Ingrese la cantidad a depositar:");
                                double montoDeposito = Double.parseDouble(deposito);
                                saldo[index] += montoDeposito;
                                JOptionPane.showMessageDialog(null,
                                        "Depósito realizado con éxito. Saldo actual: " + saldo[index]);
                                break;
                            case 1: // Retiro
                                String retiro = JOptionPane.showInputDialog(null, "Ingrese la cantidad a retirar:");
                                double montoRetiro = Double.parseDouble(retiro);
                                if (montoRetiro <= saldo[index]) {
                                    saldo[index] -= montoRetiro;
                                    JOptionPane.showMessageDialog(null,
                                            "Retiro realizado con éxito. Saldo actual: " + saldo[index]);
                                } else {
                                    JOptionPane.showMessageDialog(null, "Saldo insuficiente.");
                                }
                                break;
                            case 2: // Consulta de saldo
                                JOptionPane.showMessageDialog(null, "Su saldo actual es: " + saldo[index]);
                                break;
                            case 3: // Salir
                                JOptionPane.showMessageDialog(null, "Gracias por utilizar nuestro cajero automático.");
                                break;
                            default:
                                JOptionPane.showMessageDialog(null,
                                        "¡Opción no válida! Por favor, seleccione una opción válida del menú.");
                        }
                    } while (opcion != 3);
                    break;
                } else {
                    JOptionPane.showMessageDialog(null,
                            "Contraseña incorrecta. Por favor, intente nuevamente.");
                    intentos++;
                }
            } while (intentos < 3);
        }
        if (intentos == 3) {
            JOptionPane.showMessageDialog(null,
                    "¡Ha excedido el número máximo de intentos permitidos! La aplicación se cerrará.");
        }
    }
}
