import java.util.Scanner;

public class ejercicio20 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Ingrese el primer número entero: ");
        int num1 = input.nextInt();

        System.out.print("Ingrese el segundo número entero: ");
        int num2 = input.nextInt();

        // calculamos el MCD utilizando el algoritmo de Euclides
        int a = Math.max(num1, num2);
        int b = Math.min(num1, num2);
        while (b > 0) {
            int r = a % b;
            a = b;
            b = r;
        }
        int mcd = a;

        // calculamos el MCM utilizando la fórmula MCM = (num1 * num2) / MCD
        int mcm = (num1 * num2) / mcd;

        System.out.println("El Máximo Común Divisor (MCD) de " + num1 + " y " + num2 + " es: " + mcd);
        System.out.println("El Mínimo Común Múltiplo (MCM) de " + num1 + " y " + num2 + " es: " + mcm);
    }
}
