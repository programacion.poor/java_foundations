import java.util.Scanner;

public class ejercicio08 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Ingrese la longitud del primer lado: ");
        double lado1 = input.nextDouble();

        System.out.print("Ingrese la longitud del segundo lado: ");
        double lado2 = input.nextDouble();

        System.out.print("Ingrese la longitud del tercer lado: ");
        double lado3 = input.nextDouble();

        if (lado1 == lado2 && lado2 == lado3) {
            System.out.println("El triángulo es equilátero");
        } else if (lado1 == lado2 || lado2 == lado3 || lado1 == lado3) {
            System.out.println("El triángulo es isósceles");
        } else {
            System.out.println("El triángulo es escaleno");
        }
    }
}
