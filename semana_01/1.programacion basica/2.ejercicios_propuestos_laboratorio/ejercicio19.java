import java.util.Scanner;

public class ejercicio19 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Ingrese un número entero: ");
        int numero = input.nextInt();

        int suma = 0;
        for (int i = 1; i <= numero; i++) {
            suma += i;
        }

        System.out.println("La suma de los números enteros desde 1 hasta " + numero + " es: " + suma);
    }
}
