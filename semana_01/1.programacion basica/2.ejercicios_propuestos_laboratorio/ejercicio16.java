import java.util.Scanner;

public class ejercicio16 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Ingrese su peso en kilogramos: ");
        double peso = input.nextDouble();

        System.out.print("Ingrese su altura en metros: ");
        double altura = input.nextDouble();

        double imc = peso / (altura * altura);

        System.out.println("Su índice de masa corporal (IMC) es: " + imc);
    }
}
