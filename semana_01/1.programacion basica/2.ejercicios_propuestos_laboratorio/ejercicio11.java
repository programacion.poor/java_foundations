import java.util.Scanner;

public class ejercicio11 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Ingrese el primer número: ");
        int numero1 = input.nextInt();

        System.out.print("Ingrese el segundo número: ");
        int numero2 = input.nextInt();

        if (numero1 > numero2) {
            System.out.println(numero1 + " es el mayor número");
        } else if (numero2 > numero1) {
            System.out.println(numero2 + " es el mayor número");
        } else {
            System.out.println("Los dos números son iguales");
        }
    }
}
