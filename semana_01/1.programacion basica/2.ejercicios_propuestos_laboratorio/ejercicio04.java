import java.util.Scanner;

public class ejercicio04 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Ingrese el primer número: ");
        int num1 = input.nextInt();

        System.out.print("Ingrese el segundo número: ");
        int num2 = input.nextInt();

        int resultado = num1 * num2;

        System.out.println("El resultado de la multiplicación es: " + resultado);
    }
}
