import java.util.Scanner;

public class ejercicio02 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Ingrese su nombre: ");
        String nombre = input.nextLine();

        System.out.println("Hola, " + nombre + "!");
    }
}
