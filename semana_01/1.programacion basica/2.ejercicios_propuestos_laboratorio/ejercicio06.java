import java.util.Scanner;

public class ejercicio06 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Ingrese el dividendo: ");
        int dividendo = input.nextInt();

        System.out.print("Ingrese el divisor: ");
        int divisor = input.nextInt();

        int cociente = dividendo / divisor;
        int resto = dividendo % divisor;

        System.out.println("El cociente es: " + cociente);
        System.out.println("El resto es: " + resto);
    }
}
