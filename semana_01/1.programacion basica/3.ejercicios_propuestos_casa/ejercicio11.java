import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ejercicio11 {
    public static void main(String[] args) {
        String cadena = "La casa es grande y la puerta es roja.";
        String patron = "casa";
        
        Pattern pattern = Pattern.compile(patron);
        Matcher matcher = pattern.matcher(cadena);
        
        while (matcher.find()) {
            System.out.println("Se encontró el patrón '" + patron + "' en la posición " + matcher.start() 
                + " - " + (matcher.end() - 1) + " de la cadena: " + cadena.substring(matcher.start(), matcher.end()));
        }
    }
}
