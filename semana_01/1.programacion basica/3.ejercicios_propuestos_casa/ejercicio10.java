import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ejercicio10 {
    public static void main(String[] args) {
        String texto = "La casa es grande y la puerta es roja.";
        String patron = "casa";
        
        Pattern pattern = Pattern.compile(patron);
        Matcher matcher = pattern.matcher(texto);
        
        boolean encontrado = matcher.find();
        
        if (encontrado) {
            System.out.println("Se encontró el patrón '" + patron + "' en el texto: " + texto);
        } else {
            System.out.println("No se encontró el patrón '" + patron + "' en el texto: " + texto);
        }
    }
}
