import java.util.Locale;

public class ejercicio17 {
    public static void main(String[] args) {
        // Definimos la configuración regional de España
        Locale configuracionRegional = new Locale("es", "ES");
        
        // Imprimimos la información de la configuración regional
        System.out.println("Idioma: " + configuracionRegional.getDisplayLanguage());
        System.out.println("País: " + configuracionRegional.getDisplayCountry());
        System.out.println("Idioma y país: " + configuracionRegional.getDisplayName());
    }
}
