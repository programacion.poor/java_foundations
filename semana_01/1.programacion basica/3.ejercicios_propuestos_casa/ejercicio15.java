import java.math.BigInteger;

public class ejercicio15 {
    public static void main(String[] args) {
        BigInteger num1 = new BigInteger("12345678901234567890");
        BigInteger num2 = new BigInteger("98765432109876543210");
        
        BigInteger suma = num1.add(num2);
        BigInteger resta = num1.subtract(num2);
        BigInteger multiplicacion = num1.multiply(num2);
        BigInteger division = num1.divide(num2);
        
        System.out.println("Suma: " + suma);
        System.out.println("Resta: " + resta);
        System.out.println("Multiplicación: " + multiplicacion);
        System.out.println("División: " + division);
    }
}
