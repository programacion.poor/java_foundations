import java.util.HashMap;

public class ejercicio02 {
    public static void main(String[] args) {
        HashMap<String, Integer> personas = new HashMap<String, Integer>();
        
        personas.put("Juan", 25);
        personas.put("María", 30);
        personas.put("Pedro", 35);
        personas.put("Luisa", 28);
        
        System.out.println(personas);
    }
}
