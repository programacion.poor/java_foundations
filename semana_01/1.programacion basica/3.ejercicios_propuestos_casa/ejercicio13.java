import java.text.SimpleDateFormat;
import java.util.Date;

public class ejercicio13 {
    public static void main(String[] args) {
        Date fechaActual = new Date();
        SimpleDateFormat formatoFechaHora = new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss");
        String fechaHoraFormateada = formatoFechaHora.format(fechaActual);
        System.out.println("Fecha y hora actual con formato personalizado: " + fechaHoraFormateada);
    }
}
