public class ejercicio09 {
    public static void main(String[] args) {
        double numero1 = 10.5;
        double numero2 = 5.2;
        
        double suma = Math.addExact((int) numero1, (int) numero2);
        double resta = Math.subtractExact((int) numero1, (int) numero2);
        double producto = Math.multiplyExact((int) numero1, (int) numero2);
        double division = numero1 / numero2;
        double raizCuadrada = Math.sqrt(numero1);
        
        System.out.println("Suma: " + suma);
        System.out.println("Resta: " + resta);
        System.out.println("Producto: " + producto);
        System.out.println("División: " + division);
        System.out.println("Raíz cuadrada de " + numero1 + ": " + raizCuadrada);
    }
}
