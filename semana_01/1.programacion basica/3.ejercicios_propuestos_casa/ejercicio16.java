import java.util.TimeZone;

public class ejercicio16 {
    public static void main(String[] args) {
        // Definimos la zona horaria de Madrid
        TimeZone zonaHoraria = TimeZone.getTimeZone("latam/lima");
        
        // Imprimimos la información de la zona horaria
        System.out.println("Zona horaria: " + zonaHoraria.getDisplayName());
        System.out.println("ID: " + zonaHoraria.getID());
        System.out.println("GMT Offset: " + zonaHoraria.getRawOffset());
    }
}
