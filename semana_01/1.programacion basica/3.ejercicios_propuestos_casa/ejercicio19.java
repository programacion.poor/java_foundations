import java.io.*;

public class ejercicio19 {
    public static void main(String[] args) {
        OutputStream salida = System.out;
        
        try {
            String mensaje = "Hola, waza!";
            byte[] datos = mensaje.getBytes();
            salida.write(datos);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
