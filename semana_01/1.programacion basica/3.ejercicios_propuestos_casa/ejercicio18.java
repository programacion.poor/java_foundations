import java.io.*;

public class ejercicio18 {
    public static void main(String[] args) {
        InputStream entrada = System.in;
        
        try {
            byte[] datos = new byte[100];
            System.out.println("Introduzca una cadena de caracteres: ");
            entrada.read(datos);
            String cadena = new String(datos);
            System.out.println("La cadena introducida es: " + cadena.trim());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
