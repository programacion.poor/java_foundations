import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class ejercicio07 {
    public static void main(String[] args) {
        String rutaArchivo = "archivo.txt";
        
        try (BufferedReader br = new BufferedReader(new FileReader(rutaArchivo))) {
            String lineaActual;
            
            while ((lineaActual = br.readLine()) != null) {
                System.out.println(lineaActual);
            }
        } catch (IOException e) {
            System.out.println("Error al leer el archivo.");
            e.printStackTrace();
        }
    }
}
