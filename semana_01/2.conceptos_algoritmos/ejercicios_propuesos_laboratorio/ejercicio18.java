import java.util.Scanner;

public class ejercicio18 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int contador = 0;
        int n;
        
        System.out.println("Ingrese una lista de números enteros separados por comas:");
        String numeros = sc.nextLine();
        
        String[] lista = numeros.split(",");
        
        for (String numero : lista) {
            n = Integer.parseInt(numero);
            int suma = 0;
            int aux = n;
            while (aux > 0) {
                suma += aux % 10;
                aux /= 10;
            }
            if (n % suma == 0) {
                contador++;
            }
        }
        
        System.out.println("La cantidad de números de Harshad es: " + contador);
    }
}
