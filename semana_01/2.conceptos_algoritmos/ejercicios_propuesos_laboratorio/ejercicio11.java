import java.util.Scanner;

public class ejercicio11 {
   public static void main(String[] args) {
      Scanner sc = new Scanner(System.in);
      System.out.print("Ingrese una lista de números separados por espacios: ");
      String input = sc.nextLine();
      sc.close();
      
      String[] numeros = input.split(" ");
      int countMultiplos3 = 0;
      
      for (String num : numeros) {
         int n = Integer.parseInt(num);
         if (n % 3 == 0) {
            countMultiplos3++;
         }
      }
      
      System.out.println("Hay " + countMultiplos3 + " números múltiplos de 3 en la lista.");
   }
}
