import java.util.Scanner;

public class ejercicio08 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Ingrese una fecha en formato dd/mm/yyyy: ");
        String fecha = input.nextLine();
        String[] partesFecha = fecha.split("/");
        int dia = Integer.parseInt(partesFecha[0]);
        int mes = Integer.parseInt(partesFecha[1]);
        int anio = Integer.parseInt(partesFecha[2]);
        boolean esValida = true;
        if (anio < 0) {
            esValida = false;
        } else if (mes < 1 || mes > 12) {
            esValida = false;
        } else if (dia < 1 || dia > diasEnMes(mes, anio)) {
            esValida = false;
        }
        if (esValida) {
            System.out.println("La fecha ingresada es válida.");
        } else {
            System.out.println("La fecha ingresada no es válida.");
        }
    }

    public static int diasEnMes(int mes, int anio) {
        int dias = 0;
        switch (mes) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                dias = 31;
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                dias = 30;
                break;
            case 2:
                if (anio % 4 == 0 && (anio % 100 != 0 || anio % 400 == 0)) {
                    dias = 29;
                } else {
                    dias = 28;
                }
                break;
        }
        return dias;
    }
}
