import java.util.Scanner;

public class ejercicio16 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int numero, copia, longitud = 0, digito, suma = 0;
        
        System.out.println("Ingrese un número entero para verificar si es número de Armstrong:");
        numero = sc.nextInt();
        copia = numero;
        
        // Calculamos la longitud del número
        while (copia != 0) {
            copia = copia / 10;
            longitud++;
        }
        
        copia = numero;
        
        // Calculamos la suma de los dígitos elevados a la longitud del número
        while (copia != 0) {
            digito = copia % 10;
            suma += Math.pow(digito, longitud);
            copia /= 10;
        }
        
        // Comparamos la suma con el número original
        if (suma == numero) {
            System.out.println(numero + " es un número de Armstrong.");
        } else {
            System.out.println(numero + " no es un número de Armstrong.");
        }
    }
}
