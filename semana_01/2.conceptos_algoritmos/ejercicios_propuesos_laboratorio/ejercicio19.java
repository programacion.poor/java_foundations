import java.util.*;

public class ejercicio19 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese una lista de palabras separadas por espacios: ");
        String input = sc.nextLine();

        String[] palabras = input.split("\\s+"); // separar las palabras
        int count = 0; // contador de palabras con letras distintas

        for (String palabra : palabras) {
            Set<Character> set = new HashSet<>(); // conjunto de caracteres únicos
            for (int i = 0; i < palabra.length(); i++) {
                set.add(palabra.charAt(i));
            }
            if (set.size() == palabra.length()) { // si todos los caracteres son únicos
                count++;
            }
        }

        System.out.println("La cantidad de palabras con letras distintas es: " + count);
        sc.close();
    }
}
