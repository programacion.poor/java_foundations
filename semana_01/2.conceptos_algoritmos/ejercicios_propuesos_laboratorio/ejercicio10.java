import java.util.Scanner;

public class ejercicio10 {
   public static void main(String[] args) {
      Scanner sc = new Scanner(System.in);
      System.out.print("Ingrese una lista de números separados por espacios: ");
      String input = sc.nextLine();
      sc.close();
      
      String[] numeros = input.split(" ");
      int countPares = 0;
      
      for (String num : numeros) {
         int n = Integer.parseInt(num);
         if (n % 2 == 0) {
            countPares++;
         }
      }
      
      System.out.println("Hay " + countPares + " números pares en la lista.");
   }
}
