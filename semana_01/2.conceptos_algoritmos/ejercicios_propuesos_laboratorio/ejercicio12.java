import java.util.Scanner;

public class ejercicio12 {
   public static void main(String[] args) {
      Scanner sc = new Scanner(System.in);
      System.out.print("Ingrese una lista de números separados por espacios: ");
      String input = sc.nextLine();
      sc.close();
      
      String[] numeros = input.split(" ");
      int countNumerosPerfectos = 0;
      
      for (String num : numeros) {
         int n = Integer.parseInt(num);
         if (esNumeroPerfecto(n)) {
            countNumerosPerfectos++;
         }
      }
      
      System.out.println("Hay " + countNumerosPerfectos + " números perfectos en la lista.");
   }
   
   public static boolean esNumeroPerfecto(int n) {
      int suma = 0;
      for (int i = 1; i < n; i++) {
         if (n % i == 0) {
            suma += i;
         }
      }
      return suma == n;
   }
}
