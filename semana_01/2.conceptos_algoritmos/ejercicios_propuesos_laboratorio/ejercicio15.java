import java.util.Scanner;

public class ejercicio15 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int cantidad, contador = 0;
        String letra, palabra;
        
        System.out.println("Ingrese la letra con la que deben comenzar las palabras:");
        letra = sc.next().toLowerCase();
        
        System.out.println("Ingrese la cantidad de palabras que desea evaluar:");
        cantidad = sc.nextInt();
        
        System.out.println("Ingrese las palabras:");
        for (int i = 0; i < cantidad; i++) {
            palabra = sc.next().toLowerCase();
            if (palabra.startsWith(letra)) {
                contador++;
            }
        }
        
        System.out.println("La cantidad de palabras que comienzan con la letra " + letra.toUpperCase() + " es: " + contador);
    }
}
