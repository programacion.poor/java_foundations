import java.util.Scanner;

public class ejercicio17 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int contador = 0;
        String letra;
        char caracter;
        
        System.out.println("Ingrese una lista de palabras separadas por comas:");
        String palabras = sc.nextLine();
        
        System.out.println("Ingrese la letra a buscar:");
        letra = sc.nextLine();
        caracter = letra.charAt(0);
        
        String[] lista = palabras.split(",");
        
        for (String palabra : lista) {
            int cantidad = 0;
            for (int i = 0; i < palabra.length(); i++) {
                if (palabra.charAt(i) == caracter) {
                    cantidad++;
                }
            }
            if (cantidad == 2) {
                contador++;
            }
        }
        
        System.out.println("La cantidad de palabras que tienen la letra " + letra + " exactamente dos veces es: " + contador);
    }
}
