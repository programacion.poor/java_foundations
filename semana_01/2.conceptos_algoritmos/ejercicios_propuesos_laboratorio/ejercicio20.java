import java.util.Scanner;

public class ejercicio20 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int cantidadDivisoresMinima, contadorNumeros = 0;
        System.out.print("Ingrese el número mínimo de divisores que deben tener los números: ");
        cantidadDivisoresMinima = sc.nextInt();
        System.out.print("Ingrese la cantidad de números que desea ingresar: ");
        int cantidadNumeros = sc.nextInt();
        int[] numeros = new int[cantidadNumeros];
        for (int i = 0; i < cantidadNumeros; i++) {
            System.out.print("Ingrese el número " + (i + 1) + ": ");
            numeros[i] = sc.nextInt();
            int cantidadDivisores = contarDivisores(numeros[i]);
            if (cantidadDivisores > cantidadDivisoresMinima) {
                contadorNumeros++;
            }
        }
        System.out.println("La cantidad de números con más de " + cantidadDivisoresMinima + " divisores es: " + contadorNumeros);
    }
    
    public static int contarDivisores(int num) {
        int contadorDivisores = 0;
        for (int i = 1; i <= num; i++) {
            if (num % i == 0) {
                contadorDivisores++;
            }
        }
        return contadorDivisores;
    }
}
