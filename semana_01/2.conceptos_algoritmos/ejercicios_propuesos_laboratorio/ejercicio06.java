import java.util.Scanner;

public class ejercicio06 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Ingrese su peso en kilogramos: ");
        double peso = input.nextDouble();
        System.out.print("Ingrese su estatura en metros: ");
        double estatura = input.nextDouble();
        double imc = peso / (estatura * estatura);
        System.out.println("Su índice de masa corporal (IMC) es: " + imc);
    }
}
