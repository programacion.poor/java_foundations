import java.util.Scanner;

public class ejercicio04 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Ingrese el primer número entero: ");
        int num1 = input.nextInt();
        System.out.print("Ingrese el segundo número entero: ");
        int num2 = input.nextInt();
        System.out.print("Ingrese el tercer número entero: ");
        int num3 = input.nextInt();
        if (num1 + num2 > num3 && num1 + num3 > num2 && num2 + num3 > num1) {
            System.out.println("Los números ingresados pueden formar un triángulo válido.");
        } else {
            System.out.println("Los números ingresados no pueden formar un triángulo válido.");
        }
    }
}
