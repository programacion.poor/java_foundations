import java.util.Scanner;

public class ejercicio13 {
   public static void main(String[] args) {
      Scanner sc = new Scanner(System.in);
      System.out.print("Ingrese una lista de números separados por espacios: ");
      String input = sc.nextLine();
      sc.close();
      
      String[] numeros = input.split(" ");
      int countNumerosFibonacci = 0;
      
      for (String num : numeros) {
         int n = Integer.parseInt(num);
         if (esNumeroFibonacci(n)) {
            countNumerosFibonacci++;
         }
      }
      
      System.out.println("Hay " + countNumerosFibonacci + " números de Fibonacci en la lista.");
   }
   
   public static boolean esNumeroFibonacci(int n) {
      if (n == 0 || n == 1) {
         return true;
      }
      int a = 0;
      int b = 1;
      while (b < n) {
         int temp = b;
         b = a + b;
         a = temp;
      }
      return b == n;
   }
}
