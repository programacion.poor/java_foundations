import java.util.Scanner;

public class ejercicio03 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Ingrese el primer número entero: ");
        int num1 = input.nextInt();
        System.out.print("Ingrese el segundo número entero: ");
        int num2 = input.nextInt();
        System.out.print("Ingrese el tercer número entero: ");
        int num3 = input.nextInt();
        int mayor = num1;
        if (num2 > mayor) {
            mayor = num2;
        }
        if (num3 > mayor) {
            mayor = num3;
        }
        System.out.println("El número mayor es " + mayor);
    }
}
