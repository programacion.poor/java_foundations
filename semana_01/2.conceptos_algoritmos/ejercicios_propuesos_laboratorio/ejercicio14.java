import java.util.Scanner;

public class ejercicio14 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int cantidad, contador = 0;
        String palabra;
        
        System.out.println("Ingrese la cantidad de palabras que desea evaluar:");
        cantidad = sc.nextInt();
        
        System.out.println("Ingrese las palabras:");
        for (int i = 0; i < cantidad; i++) {
            palabra = sc.next();
            if (esPalindromo(palabra)) {
                contador++;
            }
        }
        
        System.out.println("La cantidad de palíndromos es: " + contador);
    }
    
    public static boolean esPalindromo(String palabra) {
        int i = 0, j = palabra.length() - 1;
        while (i < j) {
            if (palabra.charAt(i) != palabra.charAt(j)) {
                return false;
            }
            i++;
            j--;
        }
        return true;
    }
}
