import java.util.Scanner;

public class ejercicio07 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Ingrese una palabra o frase: ");
        String palabra = input.nextLine().toLowerCase().replaceAll("\\s+","");
        String palabraInvertida = "";
        for (int i = palabra.length() - 1; i >= 0; i--) {
            palabraInvertida += palabra.charAt(i);
        }
        if (palabra.equals(palabraInvertida)) {
            System.out.println("La palabra ingresada es un palindromo.");
        } else {
            System.out.println("la palabra ingresada no es un palindromo");
        }
    }
}
