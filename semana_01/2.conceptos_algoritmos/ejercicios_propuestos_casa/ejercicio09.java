import java.util.Scanner;

public class ejercicio09 {
   public static void main(String[] args) {
      Scanner sc = new Scanner(System.in);
      System.out.print("Introduce una lista de palabras separadas por espacios: ");
      String listaPalabras = sc.nextLine();
      String[] palabras = listaPalabras.split(" ");
      int contador = 0;
      
      for (String palabra : palabras) {
         boolean encontrado = false;
         for (char letra : palabra.toCharArray()) {
            int repeticiones = palabra.length() - palabra.replace(Character.toString(letra), "").length();
            if (repeticiones % 2 == 1) {
               encontrado = true;
               break;
            }
         }
         if (encontrado) {
            contador++;
         }
      }
      
      System.out.println("Hay " + contador + " palabras que tienen una letra repetida un número impar de veces.");
   }
}
