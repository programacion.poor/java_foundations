import java.util.Scanner;

public class ejercicio07 {
   public static boolean tieneTresLetrasConsecutivas(String palabra) {
      for (int i = 0; i < palabra.length() - 2; i++) {
         if (palabra.charAt(i) == palabra.charAt(i + 1) && palabra.charAt(i + 1) == palabra.charAt(i + 2)) {
            return true;
         }
      }
      return false;
   }
   
   public static void main(String[] args) {
      Scanner sc = new Scanner(System.in);
      System.out.print("Ingrese la cantidad de palabras: ");
      int cantidadPalabras = sc.nextInt();
      sc.nextLine(); // limpiamos el buffer
      
      int contadorPalabras = 0;
      for (int i = 0; i < cantidadPalabras; i++) {
         System.out.print("Ingrese la palabra #" + (i+1) + ": ");
         String palabra = sc.nextLine();
         if (tieneTresLetrasConsecutivas(palabra)) {
            contadorPalabras++;
         }
      }
      System.out.println("La cantidad de palabras con tres letras consecutivas es: " + contadorPalabras);
   }
}
