public class ejercicio20 {
    public static void main(String[] args) {
        int count = 0;
        for (int i = 0; i < 1000; i++) {
            int n = i;
            int sum = 0;
            int digitCount = (int) Math.floor(Math.log10(n)) + 1;
            while (n > 0) {
                int digit = n % 10;
                sum += Math.pow(digit, digitCount);
                n /= 10;
            }
            if (sum == i) {
                count++;
            }
        }
        System.out.println("El número de números de narcisismo es: " + count);
    }
}
