import java.util.Scanner;

public class ejercicio16 {
   public static void main(String[] args) {
      Scanner scanner = new Scanner(System.in);
      System.out.print("Ingresa una lista de números enteros separados por espacios: ");
      String numerosStr = scanner.nextLine();
      String[] numeros = numerosStr.split(" ");
      
      int contador = 0;
      for (String numeroStr : numeros) {
         int numero = Integer.parseInt(numeroStr);
         int suma = 0;
         int numeroTemp = numero;
         int longitud = numeroStr.length();
         while (numeroTemp > 0) {
            int digito = numeroTemp % 10;
            suma += Math.pow(digito, longitud);
            numeroTemp /= 10;
         }
         if (suma == numero) {
            contador++;
         }
      }
      
      System.out.println("Hay " + contador + " números de Armstrong en la lista.");
   }
}
