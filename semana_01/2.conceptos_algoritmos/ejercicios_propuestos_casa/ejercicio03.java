public class ejercicio03 {
    public static boolean esNumeroPerfecto(int numero) {
       int sumaDivisores = 0;
       for (int i = 1; i <= Math.sqrt(numero); i++) {
          if (numero % i == 0) {
             sumaDivisores += i + (i == 1 ? 0 : numero / i);
          }
       }
       return sumaDivisores == numero * 2;
    }
    
    public static boolean esNumeroFibonacci(int numero) {
       int a = 0, b = 1;
       while (b < numero) {
          int c = a + b;
          a = b;
          b = c;
       }
       return b == numero || numero == 0 || numero == 1;
    }
    
    public static void main(String[] args) {
       int[] numeros = {6, 15, 8, 144, 3, 21, 55};
       int contadorPerfectos = 0, contadorFibonacci = 0;
       for (int n : numeros) {
          if (esNumeroPerfecto(n)) contadorPerfectos++;
          if (esNumeroFibonacci(n)) contadorFibonacci++;
       }
       System.out.println("Hay " + contadorPerfectos + " números perfectos y " + contadorFibonacci + " números de Fibonacci.");
    }
 }
 