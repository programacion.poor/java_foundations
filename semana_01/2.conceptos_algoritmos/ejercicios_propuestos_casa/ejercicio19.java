import java.util.*;

public class ejercicio19 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese una lista de palabras separadas por espacios:");
        String input = sc.nextLine();
        String[] words = input.split(" ");
        int count = 0;
        for (String word : words) {
            HashSet<Character> uniqueChars = new HashSet<>();
            for (char c : word.toCharArray()) {
                uniqueChars.add(c);
            }
            if (uniqueChars.size() == word.length()) {
                count++;
            }
        }
        System.out.println("El número de palabras con todas las letras diferentes es: " + count);
    }
}
