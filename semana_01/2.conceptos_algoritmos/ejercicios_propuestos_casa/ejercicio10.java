public class ejercicio10 {
    public static void main(String[] args) {
       int contador = 0;
       
       for (int i = 1; i < 1000000; i++) {
          int sumaFactoriales = 0;
          int num = i;
          
          while (num != 0) {
             int digito = num % 10;
             sumaFactoriales += factorial(digito);
             num /= 10;
          }
          
          if (sumaFactoriales == i) {
             contador++;
          }
       }
       
       System.out.println("Hay " + contador + " números enteros que tienen la propiedad de ser iguales a la suma de los dígitos factoriales de su descomposición en dígitos.");
    }
    
    private static int factorial(int n) {
       int resultado = 1;
       
       for (int i = 2; i <= n; i++) {
          resultado *= i;
       }
       
       return resultado;
    }
 }
 