import java.util.Scanner;

public class ejercicio14 {
   public static void main(String[] args) {
      Scanner scanner = new Scanner(System.in);
      System.out.print("Ingresa un número entero: ");
      int numero = scanner.nextInt();
      
      int cuadrado = numero * numero;
      String cuadradoStr = Integer.toString(cuadrado);
      
      for (int i = 1; i < cuadradoStr.length(); i++) {
         int parte1 = Integer.parseInt(cuadradoStr.substring(0, i));
         int parte2 = Integer.parseInt(cuadradoStr.substring(i));
         if (parte1 + parte2 == numero && parte1 > 0 && parte2 > 0) {
            System.out.println(numero + " es un número de Kaprekar.");
            return;
         }
      }
      
      System.out.println(numero + " no es un número de Kaprekar.");
   }
}
