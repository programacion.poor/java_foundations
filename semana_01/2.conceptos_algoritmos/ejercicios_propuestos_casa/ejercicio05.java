public class ejercicio05 {
    public static boolean contieneTodasLasVocales(String palabra) {
       String vocales = "aeiou";
       for (int i = 0; i < vocales.length(); i++) {
          if (palabra.indexOf(vocales.charAt(i)) == -1) {
             return false;
          }
       }
       return true;
    }
    
    public static void main(String[] args) {
       String[] palabras = {"murcielago", "aceituno", "amigo", "aguacate", "perro", "inocuo"};
       int contador = 0;
       for (String p : palabras) {
          if (contieneTodasLasVocales(p)) contador++;
       }
       System.out.println("Hay " + contador + " palabras que contienen todas las vocales.");
    }
 }
 