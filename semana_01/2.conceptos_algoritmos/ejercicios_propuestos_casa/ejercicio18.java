public class ejercicio18 {
    public static void main(String[] args) {
        int count = 0;
        for (int i = 1; i <= 100; i++) {
            if (i % sumOfDigits(i) == 0) {
                count++;
            }
        }
        System.out.println("Hay " + count + " números de Harshad entre 1 y 100");
    }

    public static int sumOfDigits(int n) {
        int sum = 0;
        while (n > 0) {
            sum += n % 10;
            n /= 10;
        }
        return sum;
    }
}
