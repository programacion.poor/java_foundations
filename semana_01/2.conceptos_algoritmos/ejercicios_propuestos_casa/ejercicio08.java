public class ejercicio08 {
    public static int sumaDigitos(int n) {
       int suma = 0;
       while (n != 0) {
          suma += n % 10;
          n /= 10;
       }
       return suma;
    }
    
    public static void main(String[] args) {
       int suma = 0;
       for (int i = 1; i <= 100; i++) {
          suma += sumaDigitos(i);
       }
       
       for (int i = 1; i <= 100; i++) {
          if (i % suma == 0) {
             System.out.println("El número " + i + " es divisible por la suma de los dígitos de todos los números de 1 a 100");
             return;
          }
       }
       System.out.println("No hay ningún número divisible por la suma de los dígitos de todos los números de 1 a 100");
    }
 }
 