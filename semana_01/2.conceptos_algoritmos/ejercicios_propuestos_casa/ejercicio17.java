import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ejercicio17 {
    
    public static int countWordsWithTwoSameLetters(String[] words) {
        int count = 0;
        Pattern pattern = Pattern.compile("(\\w)\\w*\\1");
        for (String word : words) {
            Matcher matcher = pattern.matcher(word);
            if (matcher.find()) {
                count++;
            }
        }
        return count;
    }
    
    public static void main(String[] args) {
        String[] words = {"hola", "amigo", "arroba", "calle", "recreativo", "abuela"};
        int count = countWordsWithTwoSameLetters(words);
        System.out.println(count); // Imprime 3
    }
}
