public class ejercicio06 {
    public static boolean esDivisiblePorTodos(int numero) {
       for (int i = 1; i <= 20; i++) {
          if (numero % i != 0) {
             return false;
          }
       }
       return true;
    }
    
    public static void main(String[] args) {
       int numero = 20;
       while (!esDivisiblePorTodos(numero)) {
          numero += 20;
       }
       System.out.println("El número divisible por todos los números del 1 al 20 es: " + numero);
    }
 }
 
