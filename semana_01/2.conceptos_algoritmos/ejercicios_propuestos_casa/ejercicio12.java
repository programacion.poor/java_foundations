import java.util.Scanner;

public class ejercicio12 {
   public static void main(String[] args) {
      Scanner scanner = new Scanner(System.in);
      System.out.print("Ingresa un número entero positivo: ");
      int num = scanner.nextInt();
      
      int sumaDigitos = sumaDigitos(num);
      int factorizacionDigitos = factorizacionDigitos(num);
      
      if (sumaDigitos == factorizacionDigitos) {
         System.out.println(num + " es un número de Smith.");
      } else {
         System.out.println(num + " no es un número de Smith.");
      }
   }
   
   public static int sumaDigitos(int num) {
      int suma = 0;
      while (num > 0) {
         suma += num % 10;
         num /= 10;
      }
      return suma;
   }
   
   public static int factorizacionDigitos(int num) {
      int factorizacion = 0;
      int factor = 2;
      while (num > 1) {
         if (num % factor == 0) {
            factorizacion += sumaDigitos(factor);
            num /= factor;
         } else {
            factor++;
         }
      }
      return factorizacion;
   }
}
