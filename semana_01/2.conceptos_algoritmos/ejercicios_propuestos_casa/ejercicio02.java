public class ejercicio02 {
    public static boolean tieneVocalesConsecutivas(String palabra) {
       int contadorVocales = 0;
       for (int i = 0; i < palabra.length(); i++) {
          char letraActual = palabra.charAt(i);
          if (esVocal(letraActual)) {
             contadorVocales++;
             if (contadorVocales > 2) {
                return true;
             }
          } else {
             contadorVocales = 0;
          }
       }
       return false;
    }
    
    public static boolean esVocal(char letra) {
       return "AEIOUaeiou".indexOf(letra) != -1;
    }
    
    public static void main(String[] args) {
       String[] palabras = {"ciudad", "aeropuerto", "manzana", "creencia", "automóvil"};
       int contadorPalabras = 0;
       for (int i = 0; i < palabras.length; i++) {
          if (tieneVocalesConsecutivas(palabras[i])) {
             contadorPalabras++;
          }
       }
       System.out.println("Hay " + contadorPalabras + " palabras que tienen más de dos vocales consecutivas.");
    }
 }
 