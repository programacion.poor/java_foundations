import java.util.Scanner;

public class ejercicio13 {
   public static void main(String[] args) {
      Scanner scanner = new Scanner(System.in);
      System.out.print("Ingresa una palabra: ");
      String palabra = scanner.next().toLowerCase();
      
      int[] contadorLetras = new int[26]; // contador para cada letra del alfabeto
      for (int i = 0; i < palabra.length(); i++) {
         char letra = palabra.charAt(i);
         if (letra >= 'a' && letra <= 'z') {
            contadorLetras[letra - 'a']++;
         }
      }
      
      int contadorPalabras = 0;
      for (int i = 0; i < 26; i++) {
         if (contadorLetras[i] > 2 && contadorLetras[i] < 5) {
            contadorPalabras++;
         }
      }
      
      if (contadorPalabras > 0) {
         System.out.println(palabra + " contiene " + contadorPalabras + " letra(s) que aparecen más de dos veces pero menos de cinco veces.");
      } else {
         System.out.println(palabra + " no contiene letras que aparecen más de dos veces pero menos de cinco veces.");
      }
   }
}
