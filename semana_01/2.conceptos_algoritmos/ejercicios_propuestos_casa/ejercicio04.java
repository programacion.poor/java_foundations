public class ejercicio04 {
    public static boolean esDivisiblePorTodos(int numero) {
       for (int i = 1; i <= 10; i++) {
          if (numero % i != 0) {
             return false;
          }
       }
       return true;
    }
    
    public static void main(String[] args) {
       int[] numeros = {2520, 360, 120, 840, 90, 168, 300, 420, 72, 990};
       int contador = 0;
       for (int n : numeros) {
          if (esDivisiblePorTodos(n)) contador++;
       }
       System.out.println("Hay " + contador + " números divisibles por todos los números de 1 a 10.");
    }
 }
 