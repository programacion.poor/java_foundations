import java.util.Scanner;

public class ejercicio15 {
   public static void main(String[] args) {
      Scanner scanner = new Scanner(System.in);
      System.out.print("Ingresa una lista de palabras separadas por espacios: ");
      String palabrasStr = scanner.nextLine();
      String[] palabras = palabrasStr.split(" ");
      
      int contador = 0;
      for (String palabra : palabras) {
         if (palabra.length() >= 2 && palabra.charAt(0) == palabra.charAt(palabra.length()-1)) {
            contador++;
         }
      }
      
      System.out.println("Hay " + contador + " palabras que tienen la misma letra al principio y al final.");
   }
}
