import java.util.HashSet;

public class ejercicio01 {
   public static boolean tieneSuma(int[] numeros, int suma) {
      HashSet<Integer> numerosComplementarios = new HashSet<Integer>();
      for (int i = 0; i < numeros.length; i++) {
         int complemento = suma - numeros[i];
         if (numerosComplementarios.contains(complemento)) {
            return true;
         }
         numerosComplementarios.add(numeros[i]);
      }
      return false;
   }
   
   public static void main(String[] args) {
      int[] numeros = {3, 5, 8, 11, 13};
      int suma = 16;
      
      if (tieneSuma(numeros, suma)) {
         System.out.println("Hay al menos un par de números cuya suma es " + suma);
      } else {
         System.out.println("No hay ningún par de números cuya suma sea " + suma);
      }
   }
}
