import java.util.Scanner;

public class ejercicio05 {
    public static void main(String[] args) {
        Scanner wz = new Scanner(System.in);
        double radio, longitud, area;
        final double PI = 3.14159265359;

        System.out.print("Introduce el valor del radio de la circunferencia: ");
        radio = wz.nextDouble();

        longitud = 2 * PI * radio;
        area = PI * radio * radio;

        System.out.println("La longitud de la circunferencia es: " + longitud);
        System.out.println("El área de la circunferencia es: " + area);
    }
}
