import java.util.Scanner;

public class ejercicio14 {
    public static void main(String[] args) {
        Scanner wz = new Scanner(System.in);
        double precioBase, impuesto, margenGanancia, precioFinal;

        System.out.print("Introduce el precio base del producto: ");
        precioBase = wz.nextDouble();

        System.out.print("Introduce el impuesto en porcentaje (0 si no tiene): ");
        impuesto = wz.nextDouble();

        System.out.print("Introduce el margen de ganancia en porcentaje: ");
        margenGanancia = wz.nextDouble();

        precioFinal = precioBase * (1 + impuesto / 100) * (1 + margenGanancia / 100);

        System.out.println("El precio final de venta es: " + precioFinal);
    }
}
