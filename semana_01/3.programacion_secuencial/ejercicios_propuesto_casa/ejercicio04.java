import java.util.Scanner;

public class ejercicio04 {
    public static void main(String[] args) {
        Scanner wz = new Scanner(System.in);
        double celsius, fahrenheit;

        System.out.print("Introduce una temperatura en grados Celsius: ");
        celsius = wz.nextDouble();

        fahrenheit = 32 + (9 * celsius / 5);

        System.out.println(celsius + " grados Celsius equivalen a " + fahrenheit + " grados Fahrenheit.");
    }
}
