import java.util.Scanner;

public class ejercicio10 {
    public static void main(String[] args) {
        Scanner wz = new Scanner(System.in);
        int numero;

        System.out.print("Introduce un número de 3 cifras: ");
        numero = wz.nextInt();

        int cifra1 = numero / 100;
        int cifra2 = (numero % 100) / 10;
        int cifra3 = numero % 10;

        System.out.println("La primera cifra es: " + cifra1);
        System.out.println("La segunda cifra es: " + cifra2);
        System.out.println("La tercera cifra es: " + cifra3);
    }
}
