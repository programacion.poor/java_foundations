import java.util.Scanner;

public class ejercicio17 {
    public static void main(String[] args) {
        Scanner wz = new Scanner(System.in);
        double celsius, kelvin;
        String respuesta;

        do {
            System.out.print("Introduce la temperatura en grados Celsius: ");
            celsius = wz.nextDouble();

            kelvin = celsius + 273.15;

            System.out.println(celsius + " grados Celsius son equivalentes a " + kelvin + " grados Kelvin.");

            System.out.print("Repetir proceso? (S/N): ");
            respuesta = wz.next();
        } while (respuesta.equalsIgnoreCase("S"));
    }
}
