import java.util.Scanner;

public class ejercicio08 {
    public static void main(String[] args) {
        Scanner wz = new Scanner(System.in);
        double radio, volumen;
        final double PI = 3.14159265359;

        System.out.print("Introduce el valor del radio de la esfera: ");
        radio = wz.nextDouble();

        volumen = 4.0 / 3.0 * PI * Math.pow(radio, 3);

        System.out.println("El volumen de la esfera es: " + volumen);
    }
}
