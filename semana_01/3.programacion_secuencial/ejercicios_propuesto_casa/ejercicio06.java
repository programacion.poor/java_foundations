import java.util.Scanner;

public class ejercicio06 {
    public static void main(String[] args) {
        Scanner wz = new Scanner(System.in);
        double kmh, ms;

        System.out.print("Introduce una velocidad en km/h: ");
        kmh = wz.nextDouble();

        ms = kmh * 1000 / 3600;

        System.out.println(kmh + " km/h equivalen a " + ms + " m/s.");
    }
}
