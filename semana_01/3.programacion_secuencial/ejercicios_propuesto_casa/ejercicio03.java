import java.util.Scanner;

public class ejercicio03 {
    public static void main(String[] args) {
        Scanner wz = new Scanner(System.in);
        int numero;

        System.out.print("Introduce un número entero: ");
        numero = wz.nextInt();

        int doble = numero * 2;
        int triple = numero * 3;

        System.out.println("El doble de " + numero + " es: " + doble);
        System.out.println("El triple de " + numero + " es: " + triple);
    }
}
