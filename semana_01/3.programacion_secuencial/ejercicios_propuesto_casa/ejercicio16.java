import java.util.Scanner;

public class ejercicio16 {
    public static void main(String[] args) {
        Scanner wz = new Scanner(System.in);
        double celsius, kelvin, reamur;

        System.out.print("Introduce la temperatura en grados Celsius: ");
        celsius = wz.nextDouble();

        kelvin = celsius + 273.15;
        reamur = celsius * 4 / 5;

        System.out.println(celsius + " grados Celsius son equivalentes a:");
        System.out.println(kelvin + " grados Kelvin");
        System.out.println(reamur + " grados Reamur");
    }
}
