import java.util.Scanner;

public class ejercicio07 {
    public static void main(String[] args) {
        Scanner wz = new Scanner(System.in);
        double cateto1, cateto2, hipotenusa;

        System.out.print("Introduce la longitud del primer cateto: ");
        cateto1 = wz.nextDouble();

        System.out.print("Introduce la longitud del segundo cateto: ");
        cateto2 = wz.nextDouble();

        hipotenusa = Math.sqrt(cateto1 * cateto1 + cateto2 * cateto2);

        System.out.println("La longitud de la hipotenusa es: " + hipotenusa);
    }
}
