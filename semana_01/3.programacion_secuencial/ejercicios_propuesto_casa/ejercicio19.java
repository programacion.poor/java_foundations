import java.util.Scanner;

public class ejercicio19 {
    public static void main(String[] args) {
        Scanner wz = new Scanner(System.in);
        int contador = 0;
        int numero;

        System.out.print("Introduce números enteros (introduce 0 para finalizar): ");
        do {
            numero = wz.nextInt();
            if (numero % 10 == 2) {
                contador++;
            }
        } while (numero != 0);

        System.out.println("Se introdujeron " + (contador - 1) + " números que terminan en 2.");
    }
}
