import java.util.Scanner;

public class ejercicio01 {
    public static void main(String[] args) {
        Scanner wz = new Scanner(System.in);
        int num1, num2;

        System.out.print("Introduce el primer número: ");
        num1 = wz.nextInt();
        System.out.print("Introduce el segundo número: ");
        num2 = wz.nextInt();

        System.out.println("Los números introducidos son: " + num1 + " y " + num2);
    }
}
