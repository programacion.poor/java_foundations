import java.util.Scanner;

public class ejercicio12 {
    public static void main(String[] args) {
        Scanner wz = new Scanner(System.in);
        int numero;

        System.out.print("Introduce un número de 5 cifras: ");
        numero = wz.nextInt();

        int cifra5 = numero % 10;
        int cifra4 = (numero % 100) / 10;
        int cifra3 = (numero % 1000) / 100;
        int cifra2 = (numero % 10000) / 1000;
        int cifra1 = numero / 10000;

        System.out.println("Las cifras del número son: " + cifra5 + " " + cifra4 + " " + cifra3 + " " + cifra2 + " " + cifra1);
    }
}
