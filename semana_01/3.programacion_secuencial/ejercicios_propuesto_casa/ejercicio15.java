import java.util.Scanner;

public class ejercicio15 {
    public static void main(String[] args) {
        Scanner wz = new Scanner(System.in);
        int N, m, resultado;

        System.out.print("Introduce el valor de N: ");
        N = wz.nextInt();

        System.out.print("Introduce el número de cifras que deseas eliminar: ");
        m = wz.nextInt();

        resultado = N / (int) Math.pow(10, m); // División entera
        resultado = resultado * (int) Math.pow(10, m); // Truncamiento a un número entero

        System.out.println("El resultado es: " + resultado);
    }
}
