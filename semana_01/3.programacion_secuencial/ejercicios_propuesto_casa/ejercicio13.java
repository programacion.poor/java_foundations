import java.util.Scanner;

public class ejercicio13 {
    public static void main(String[] args) {
        Scanner wz = new Scanner(System.in);
        int dia, mes, anio, sum, dig;

        System.out.println("Introduce tu fecha de nacimiento:");
        System.out.print("Día (2 cifras): ");
        dia = wz.nextInt();
        System.out.print("Mes (2 cifras): ");
        mes = wz.nextInt();
        System.out.print("Año (4 cifras): ");
        anio = wz.nextInt();

        sum = dia + mes + anio;
        while (sum > 9) {
            dig = sum % 10;
            sum = sum / 10;
            sum = sum + dig;
        }

        System.out.println("Tu número de la suerte es: " + sum);
    }
}
