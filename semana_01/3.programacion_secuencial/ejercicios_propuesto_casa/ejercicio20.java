import java.util.Scanner;

public class ejercicio20 {
    public static void main(String[] args) {
        Scanner wz = new Scanner(System.in);

        System.out.print("Introduce el primer número: ");
        int num1 = wz.nextInt();

        System.out.print("Introduce el segundo número: ");
        int num2 = wz.nextInt();

        int sumaDivisores1 = sumarDivisoresPropios(num1);
        int sumaDivisores2 = sumarDivisoresPropios(num2);

        if (sumaDivisores1 == num2 && sumaDivisores2 == num1) {
            System.out.println(num1 + " y " + num2 + " son amigos.");
        } else {
            System.out.println(num1 + " y " + num2 + " no son amigos.");
        }
    }

    public static int sumarDivisoresPropios(int num) {
        int suma = 0;

        for (int i = 1; i <= num / 2; i++) {
            if (num % i == 0) {
                suma += i;
            }
        }

        return suma;
    }
}
