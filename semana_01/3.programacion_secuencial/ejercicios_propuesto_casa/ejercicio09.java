import java.util.Scanner;

public class ejercicio09 {
    public static void main(String[] args) {
        Scanner wz = new Scanner(System.in);
        double lado1, lado2, lado3, semiperimetro, area;

        System.out.print("Introduce la longitud del primer lado: ");
        lado1 = wz.nextDouble();

        System.out.print("Introduce la longitud del segundo lado: ");
        lado2 = wz.nextDouble();

        System.out.print("Introduce la longitud del tercer lado: ");
        lado3 = wz.nextDouble();

        semiperimetro = (lado1 + lado2 + lado3) / 2;

        area = Math.sqrt(semiperimetro * (semiperimetro - lado1) * (semiperimetro - lado2) * (semiperimetro - lado3));

        System.out.println("El área del triángulo es: " + area);
    }
}
