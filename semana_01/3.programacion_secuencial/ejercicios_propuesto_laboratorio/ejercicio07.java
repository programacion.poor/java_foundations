import java.util.Scanner;

public class ejercicio07 {
   public static void main(String[] args) {
      Scanner wz = new Scanner(System.in);
      System.out.print("Ingrese un número entero: ");
      int num = wz.nextInt();

      int valorAbsoluto = Math.abs(num);
      System.out.println("El valor absoluto de " + num + " es: " + valorAbsoluto);
   }
}
