import java.util.Scanner;

public class ejercicio05 {
   public static void main(String[] args) {
      Scanner wz = new Scanner(System.in);
      System.out.print("Ingrese el número de lados del polígono: ");
      int numLados = wz.nextInt();
      System.out.print("Ingrese la longitud de un lado del polígono: ");
      double longitudLado = wz.nextDouble();

      double perimetro = numLados * longitudLado;
      System.out.println("El perímetro del polígono regular es: "+perimetro);
   }
}
