import java.util.Scanner;

public class ejercicio13 {
    public static void main(String[] args) {
        Scanner wz = new Scanner(System.in);
        System.out.print("Ingrese la temperatura en grados Celsius (°C): ");
        double celsius = wz.nextDouble();

        double fahrenheit = (celsius * 9/5) + 32;

        System.out.println(celsius + " grados Celsius equivale a " + fahrenheit + " grados Fahrenheit (°F)");
    }
}
