import java.util.Scanner;

public class ejercicio03 {
   public static void main(String[] args) {
      Scanner wz = new Scanner(System.in);
      System.out.print("Ingrese el valor del primer cateto: ");
      double cateto1 = wz.nextDouble();
      System.out.print("Ingrese el valor del segundo cateto: ");
      double cateto2 = wz.nextDouble();

      double hipotenusa = Math.sqrt(Math.pow(cateto1, 2) + Math.pow(cateto2, 2));
      System.out.println("El valor de la hipotenusa es: " + hipotenusa);
   }
}
