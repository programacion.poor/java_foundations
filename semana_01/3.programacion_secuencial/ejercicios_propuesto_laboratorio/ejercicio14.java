import java.util.Scanner;

public class ejercicio14 {
    public static void main(String[] args) {
        Scanner wz = new Scanner(System.in);
        System.out.print("Ingrese el precio del producto a comprar: ");
        double presupuesto = wz.nextDouble();
  
        
        double descuento = presupuesto * 0.15;
        double montoAPagar = presupuesto - descuento;
        
        System.out.println("El descuento es: $" + descuento);
        System.out.println("El monto a pagar finalmente es: $" + montoAPagar);
     }
  }