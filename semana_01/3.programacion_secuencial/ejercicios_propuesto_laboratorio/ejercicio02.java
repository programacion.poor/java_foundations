import java.util.Scanner;

public class ejercicio02 {
   public static void main(String[] args) {
      Scanner wz = new Scanner(System.in);
      System.out.print("Ingrese un número entero: ");
      int num = wz.nextInt();
      wz.close();
      
      boolean esPrimo = true;
      
      if (num <= 1) {
         esPrimo = false;
      } else {
         for (int i = 2; i <= Math.sqrt(num); i++) {
            if (num % i == 0) {
               esPrimo = false;
               break;
            }
         }
      }
      
      if (esPrimo) {
         System.out.println(num + " es un número primo.");
      } else {
         System.out.println(num + " no es un número primo.");
      }
   }
}
