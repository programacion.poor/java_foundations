import java.util.Scanner;

public class ejercicio12 {
   public static void main(String[] args) {
      Scanner wz = new Scanner(System.in);
      System.out.print("Ingrese el presupuesto anual del hospital: ");
      double presupuesto = wz.nextDouble();

      double presupuestoOncologia = presupuesto * 0.55;
      double presupuestoPedriatria = presupuesto * 0.20;
      double presupuestoTraumatologia = presupuesto * 0.25;

      System.out.println("El presupuesto anual de Oncología es: " + presupuestoOncologia);
      System.out.println("El presupuesto anual de Pediatría es: " + presupuestoPedriatria);
      System.out.println("El presupuesto anual de Traumatología es: " + presupuestoTraumatologia);
   }
}
