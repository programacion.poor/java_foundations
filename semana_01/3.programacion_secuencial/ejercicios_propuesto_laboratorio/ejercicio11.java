import java.util.Scanner;

public class ejercicio11 {
   public static void main(String[] args) {
      Scanner wz = new Scanner(System.in);
      System.out.print("Ingrese el nombre del vendedor: ");
      String nombre = wz.nextLine();
      System.out.print("Ingrese el número de equipos exclusivos vendidos: ");
      int equipos = wz.nextInt();
      System.out.print("Ingrese el valor total facturado: ");
      double valor = wz.nextDouble();

      double comision = 50.0 * equipos + 0.07 * valor;
      double salarioTotal = 2500.0 + comision;

      System.out.println("El salario total de " + nombre + " es de S/" + salarioTotal);
   }
}
