import java.util.Scanner;

public class ejercicio04 {
   public static void main(String[] args) {
      Scanner wz = new Scanner(System.in);
      System.out.print("Ingrese el valor del primer lado: ");
      double lado1 = wz.nextDouble();
      System.out.print("Ingrese el valor del segundo lado: ");
      double lado2 = wz.nextDouble();
      System.out.print("Ingrese el valor del tercer lado: ");
      double lado3 = wz.nextDouble();

      double s = (lado1 + lado2 + lado3) / 2;
      double area = Math.sqrt(s * (s - lado1) * (s - lado2) * (s - lado3));
      System.out.println("El área del triángulo es: " + area);
   }
}
