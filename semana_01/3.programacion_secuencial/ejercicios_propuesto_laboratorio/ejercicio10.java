import java.util.Scanner;

public class ejercicio10 {
   public static void main(String[] args) {
      Scanner wz = new Scanner(System.in);

      System.out.print("Ingrese el grado del polinomio (menor o igual a 7): ");
      int grado = wz.nextInt();

      while (grado > 7) {
         System.out.print("El grado del polinomio debe ser menor o igual a 7. Ingrese nuevamente: ");
         grado = wz.nextInt();
      }

      double[] coeficientes = new double[grado + 1];
      for (int i = 0; i <= grado; i++) {
         System.out.print("Ingrese el coeficiente de x^" + i + ": ");
         coeficientes[i] = wz.nextDouble();
      }

      System.out.print("El polinomio es: ");
      for (int i = grado; i >= 0; i--) {
         if (coeficientes[i] != 0) {
            if (i == grado) {
               System.out.print(coeficientes[i] + "x^" + i);
            } else if (i == 1) {
               if (coeficientes[i] > 0) {
                  System.out.print(" + " + coeficientes[i] + "x");
               } else {
                  System.out.print(" - " + Math.abs(coeficientes[i]) + "x");
               }
            } else if (i == 0) {
               if (coeficientes[i] > 0) {
                  System.out.print(" + " + coeficientes[i]);
               } else {
                  System.out.print(" - " + Math.abs(coeficientes[i]));
               }
            } else {
               if (coeficientes[i] > 0) {
                  System.out.print(" + " + coeficientes[i] + "x^" + i);
               } else {
                  System.out.print(" - " + Math.abs(coeficientes[i]) + "x^" + i);
               }
            }
         }
      }
   }
}
