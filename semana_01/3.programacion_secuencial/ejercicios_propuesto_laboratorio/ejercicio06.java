import java.util.Scanner;

public class ejercicio06 {
   public static void main(String[] args) {
      Scanner wz = new Scanner(System.in);
      System.out.print("Ingrese un número entero: ");
      int num = wz.nextInt();

      double raizCuadrada = Math.sqrt((double) num);
      System.out.println("La raíz cuadrada de " + num + " es: " + raizCuadrada);
   }
}
