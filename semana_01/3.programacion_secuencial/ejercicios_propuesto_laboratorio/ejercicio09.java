import java.util.Scanner;

public class ejercicio09 {
   public static void main(String[] args) {
      Scanner wz = new Scanner(System.in);
      System.out.print("Ingrese la longitud del primer lado: ");
      double l1 = wz.nextDouble();
      System.out.print("Ingrese la longitud del segundo lado: ");
      double l2 = wz.nextDouble();
      System.out.print("Ingrese la longitud del tercer lado: ");
      double l3 = wz.nextDouble();

      double hipotenusa = 0.0;
      double cateto1 = 0.0;
      double cateto2 = 0.0;

      if (l1 >= l2 && l1 >= l3) {
         hipotenusa = l1;
         cateto1 = l2;
         cateto2 = l3;
      } else if (l2 >= l1 && l2 >= l3) {
         hipotenusa = l2;
         cateto1 = l1;
         cateto2 = l3;
      } else if (l3 >= l1 && l3 >= l2) {
         hipotenusa = l3;
         cateto1 = l1;
         cateto2 = l2;
      }

      if (hipotenusa * hipotenusa == cateto1 * cateto1 + cateto2 * cateto2) {
         double seno = cateto1 / hipotenusa;
         double coseno = cateto2 / hipotenusa;
         double tangente = cateto1 / cateto2;
         System.out.println("El triángulo es rectángulo.");
         System.out.println("Seno del ángulo agudo: " + seno);
         System.out.println("Coseno del ángulo agudo: " + coseno);
         System.out.println("Tangente del ángulo agudo: " + tangente);
      } else {
         System.out.println("Los lados ingresados no forman un triángulo rectángulo.");
      }
   }
}
