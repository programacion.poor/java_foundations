import java.util.Scanner;

public class ejercicio01 {
    public static void main(String[] args) {
        Scanner wz = new Scanner(System.in);
        System.out.print("Ingrese el valor del radio mayor: ");
        double radioMayor = wz.nextDouble();
        System.out.print("Ingrese el valor del radio menor: ");
        double radioMenor = wz.nextDouble();
        double area = Math.PI * radioMayor * radioMenor;
        double perimetro = 2 * Math.PI * Math.sqrt((Math.pow(radioMayor, 2) + Math.pow(radioMenor, 2)) / 2);
        System.out.println("El área de la elipse es: " + area);
        System.out.println("El perímetro de la elipse es: " + perimetro);
    }
}
