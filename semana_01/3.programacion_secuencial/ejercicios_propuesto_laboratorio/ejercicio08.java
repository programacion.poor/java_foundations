import java.util.Scanner;

public class ejercicio08 {
   public static void main(String[] args) {
      Scanner wz = new Scanner(System.in);
      System.out.print("Ingrese la longitud del primer lado: ");
      double l1 = wz.nextDouble();
      System.out.print("Ingrese la longitud del segundo lado: ");
      double l2 = wz.nextDouble();
      System.out.print("Ingrese la longitud del tercer lado: ");
      double l3 = wz.nextDouble();
      System.out.print("Ingrese la longitud del cuarto lado: ");
      double l4 = wz.nextDouble();
      System.out.print("Ingrese la alt del trapecio: ");
      double alt = wz.nextDouble();

      double BMayor = 0.0;
      double BMenor = 0.0;

      if (l1 == l3) {
         BMayor = l1;
         BMenor = l2;
      } else if (l2 == l4) {
         BMayor = l2;
         BMenor = l1;
      }

      double area = ((BMayor + BMenor) / 2.0) * alt;
      System.out.println("El área del trapecio es: "+area);
   }
}
